# Framework for Survey Desktop Application

My Final Year Project (FYP) for my degree course. Which is creating a framework for survey desktop application.  
Prototype included.  
Uses SBT to compile and run.  

# Dependencies
spray-json  
Akka  
Akka HTTP  
Scala  
Java  
Joda Time 


# Credits
FYP supervisor: Dr Chin Teck Min for the survey models.
