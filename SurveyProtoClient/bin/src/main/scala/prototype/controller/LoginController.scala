package prototype.controller

import scalafxml.core.macros.sfxml

import scalafx.scene.control. { TextField, PasswordField, Button, Alert, ButtonType }
import scalafx.event.ActionEvent
import scalafx.stage.Stage
import scalafx.scene.input.KeyEvent
import scalafx.Includes._

@sfxml
class LoginController(
    UsernameText: TextField,
    PasswordText: PasswordField,
    LoginButton: Button,
    CancelButton: Button) {
	
  UsernameText.requestFocus
  private var myOwnStage: Stage = null
  
  def buttonHandler(e: ActionEvent) {
    val button = e.source.asInstanceOf[javafx.scene.control.Button]
    button.id.value match {
      case "LoginButton" => {
	  val username = UsernameText.text.value
	  val password = PasswordText.text.value
        if (username.contains("@") && username.contains('.') && !password.isEmpty) {
          prototype.Prototype.changeScene(username, password)
          if (myOwnStage != null) {
            myOwnStage.close
            myOwnStage = null
          }
        } else {
          new Alert(Alert.AlertType.Error, "Please enter a valid email address or make sure the password is filled", ButtonType.OK).showAndWait
        }
      }
      case "CancelButton" => {
        prototype.Prototype.stage.close
      }
    }
  }
  
  def passStage(stage: Stage) {
    myOwnStage = stage
  }
  
  def textEnterKey(e: ActionEvent) {
    val text = e.source.asInstanceOf[javafx.scene.control.TextField]
    text.id.value match {
      case "UsernameText" => PasswordText.requestFocus
      case "PasswordText" => LoginButton.fire
    }
  }
  
  def textHandler(e: KeyEvent) {
  }
}