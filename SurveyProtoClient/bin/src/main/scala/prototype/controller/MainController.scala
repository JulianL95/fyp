package prototype.controller

import scalafx.Includes._
import scalafx.scene.layout.{ VBox, AnchorPane, BorderPane }
import scalafx.scene.control.{ Button, SplitPane, Menu, MenuItem, TextArea, ListView, Alert }
import scalafx.scene.control.cell.TextFieldListCell
import scalafx.scene.input.{ InputEvent, MouseEvent, KeyEvent, MouseButton }
import scalafx.event.{ ActionEvent }
import scalafx.stage.{ Stage, Modality }
import scalafx.scene.{ Scene, Node }
import scalafx.collections.ObservableBuffer

import scalafxml.core.{NoDependencyResolver, FXMLLoader}
import scalafxml.core.macros.sfxml

import scala.collection.mutable. { ArrayBuffer, ListMap }
import scala.concurrent.duration._
import scala.concurrent.Future

import java.io.{ File, FileReader, BufferedReader }
import java.nio.charset.Charset
import java.lang.reflect.Field

import org.joda.time.DateTime

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._

import spray.json._

import api.models.questionaireanswermodel._
import api.models.questionaireanswermodel.AnswerJsonProtocol._
import api.models._
import api.models.authdetailsmodel._
import api.utils.Utilities
import api.linkage.{ Connection, Communication }
import api.engine._
import prototype.Prototype
import prototype.result.ResultSubmitter

@sfxml
class MainController (
    MainPane: BorderPane,
    SurveyDetailsPane: BorderPane,
    SurveyDetailsAnchorPane: AnchorPane,
    ListingsPane: SplitPane,
    SurveyListOwned: ListView[String],
    SaveButton: Button,
    AddNewButton: Button,
    EditButton: Button,
    FileMenuButton: Menu,
    AboutMenuButton: MenuItem,
    CloseMenuButton: MenuItem,
    HowToMenuButton: MenuItem,
    NewAnswerMenu: MenuItem,
    SaveMenuButton: MenuItem,
    StatusLog: TextArea,
    SurveyDetails: AnchorPane) {
  
  val aboutLoader = new FXMLLoader(getClass.getClassLoader.getResource("prototype/view/About.fxml"), NoDependencyResolver )
  aboutLoader.load
  val aboutRoot = aboutLoader.getRoot[javafx.scene.layout.BorderPane]
  val helpLoader = new FXMLLoader(getClass.getClassLoader.getResource("prototype/view/Help.fxml"), NoDependencyResolver )
  helpLoader.load
  val helpRoot = helpLoader.getRoot[javafx.scene.layout.BorderPane]
  
  val chartLoader = new FXMLLoader(getClass.getClassLoader.getResource("prototype/view/Charts.fxml"), NoDependencyResolver )
  chartLoader.load
  val chartRoot = chartLoader.getRoot[javafx.scene.layout.BorderPane]
  
  val chartControl = chartLoader.getController[ChartController#Controller]
  println(Thread.currentThread.getName, "controller")
  
  val saved = new File("projects_answers")
  saved.mkdir
  
  val listViewItems = new ObservableBuffer[String]()
  SurveyListOwned.items = listViewItems
  
  var surveystuff: Survey = null
  var engineObj: QuestionTypeDisplayGenerator = null
  var vBoxAndId: (VBox, ArrayBuffer[String]) = null
  var openStage: Stage = null
  var clientDetails: ClientSessionDetails = null
  var connection: Connection = null
  var resultSubmitter: ResultSubmitter = null
  
  var location: Array[Option[Boolean]] = null
  var doingOnNet: ListMap[String, Option[Boolean]] = null
  var onlineDevice: ListMap[String, Option[Boolean]] = null
  var characterize: ListMap[String, Int] = null
  var apply: ListMap[String, Int] = null
  var describe: ListMap[String, Int] = null
  var answers: Array[Option[String]] = null
  var tempFileName = ""
  var usingFileName = ""
  var resultObj: SurveyAnswer[ExcessiveInternetSurvey] = null
  var openedStage = new Stage {
    resizable = false
    scene = new Scene(600, 600)
  }
  var chartStage = new Stage {
    resizable = false
    scene = new Scene{
      root = chartRoot
    }
  }
  var loginStage = new Stage {
    resizable = false
    title = "Login"
  }
  loginStage.initModality(Modality.ApplicationModal)
  loginStage.initOwner(Prototype.stage)
  var answerArrayBuffer = ArrayBuffer[SurveyAnswer[ExcessiveInternetSurvey]]()
  val waitFlag = AnyRef
  
  def initializeAllResultArray {
    resultObj = null
    usingFileName = ""
    tempFileName = ""
    location = Array[Option[Boolean]](
      Some(false),
      Some(false)
    )
    doingOnNet = ListMap[String, Option[Boolean]](
      "searchInfo" -> Some(false),
    	"readNews" -> Some(false),
    	"writeForum" -> Some(false),
    	"useSocial" -> Some(false),
    	"playgame" -> Some(false)    
    )
    onlineDevice = ListMap[String, Option[Boolean]](
      "pc" -> Some(false),
    	"laptop" -> Some(false),
    	"tablet" -> Some(false),
    	"mobile" -> Some(false)
    )
    characterize = ListMap[String, Int](
        "characterize1" -> 0,
        "characterize2" -> 0,
        "characterize3" -> 0,
        "characterize4" -> 0,
        "characterize5" -> 0,
        "characterize6" -> 0,
        "characterize7" -> 0,
        "characterize8" -> 0,
        "characterize9" -> 0,
        "characterize10" -> 0,
        "characterize11" -> 0,
        "characterize12" -> 0,
        "characterize13" -> 0,
        "characterize14" -> 0,
        "characterize15" -> 0,
        "characterize16" -> 0,
        "characterize17" -> 0,
        "characterize18" -> 0
    )
    apply = ListMap[String, Int](
        "apply1" -> 0,
        "apply2" -> 0,
        "apply3" -> 0,
        "apply4" -> 0,
        "apply5" -> 0,
        "apply6" -> 0,
        "apply7" -> 0,
        "apply8" -> 0,
        "apply9" -> 0,
        "apply10" -> 0,
        "apply11" -> 0,
        "apply12" -> 0,
        "apply13" -> 0,
        "apply14" -> 0,
        "apply15" -> 0,
        "apply16" -> 0,
        "apply17" -> 0,
        "apply18" -> 0,
        "apply19" -> 0,
        "apply20" -> 0,
        "apply21" -> 0
    )
    describe = ListMap[String, Int](
        "describey1" -> 0,
        "describey2" -> 0,
        "describey3" -> 0,
        "describey4" -> 0,
        "describey5" -> 0,
        "describey6" -> 0,
        "describey7" -> 0,
        "describey8" -> 0,
        "describey9" -> 0,
        "describey10" -> 0,
        "describey11" -> 0,
        "describey12" -> 0,
        "describey13" -> 0,
        "describey14" -> 0,
        "describey15" -> 0,
        "describey16" -> 0,
        "describey17" -> 0,
        "describey18" -> 0,
        "describey19" -> 0,
        "describey20" -> 0
    )
    answers = new Array[Option[String]](11)
    answers = answers.zipWithIndex.map {
      case (_, i) => {
        if (i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 8) {
          Some("0")
        } else {
          Some("")
        }
      }
    }
  }
  
  SurveyListOwned.cellFactory = (SurveyListOwned) => {
    new TextFieldListCell[String]{
      onMouseClicked = { e:MouseEvent => listViewClicked(e) }
    }
  }
  
  new File("projects_answers").mkdir
  val players = new File("projects_answers").listFiles
  for(x <- players){
    if (x.toString.contains(".srl")) {
      val nameOfFileWithExtension = x.getName
      val nameOfFile = nameOfFileWithExtension.split('.')(0)
      listViewItems+=nameOfFile
    }
  }
  
  def startUp (stage: Stage, firstTime: Boolean, client: ClientSessionDetails, submitter: ResultSubmitter, loginPane: AnchorPane){
    println(loginPane)
    loginStage.scene = new Scene { root = loginPane }
    SurveyDetailsPane.visible = false
    ListingsPane.visible = true
    SaveMenuButton.disable = true
    openStage = stage
    clientDetails = client
    resultSubmitter = submitter
    initializeAllResultArray
    initialize
  }
  
  def initialize {
    SurveyDetailsPane.visible = false
    ListingsPane.visible = true
    SaveMenuButton.disable = true
    SurveyDetailsAnchorPane.children.clear
    println(SurveyDetailsAnchorPane.children.length)
    surveystuff = ExcessiveInternetSurveySample.survey
    engineObj = QuestionTypeDisplayGenerator(width = openStage.width.toInt-50)
    vBoxAndId = engineObj.generateRootLayoutSurvey(surveystuff, surveyControlsHandler)
    listHandler(false)
  }
  
  def listHandler (failed: Boolean, filename: String = ""){
    if (failed) {
      new Alert (Alert.AlertType.Error) {
        title = "Edit Error"
        headerText = "Unable to edit"
        if (filename.isEmpty) {
          contentText = "Edit failed\nReasons: file has already been submitted"
        } else {
          contentText = "Edit failed\nReasons: file has already been submitted\nFilename: " + filename
        }
      }.showAndWait
    }
    listViewItems.clear
    answerArrayBuffer.clear
    new File("projects_answers").mkdir
    val players = new File("projects_answers").listFiles
    for(x <- players){
      if (x.toString.contains(".srl")) {
        val nameOfFileWithExtension = x.getName
        val nameOfFile = nameOfFileWithExtension.split('.')(0)
        answerArrayBuffer+=Utilities.loadSerializedObject[SurveyAnswer[ExcessiveInternetSurvey]](new File("projects_answers").toString, nameOfFile).get
        listViewItems+=nameOfFile
      }
    }
  }
  
  def surveyControlsHandler(e: InputEvent) = {
    val control = e.source.asInstanceOf[javafx.scene.Node]
    val controlId = control.id.value
    val splitId = controlId.split("-")
    val name = splitId(0)
    var answer = ""
    
    try{
      e.source.asInstanceOf[javafx.scene.control.RadioButton]
      answer = splitId(1)
    } catch {
      case e: Exception => {
        answer = ""
      }
    }
    if (answer.equals("")) {
      try{
        e.source.asInstanceOf[javafx.scene.control.CheckBox]
        answer = splitId(1)
      } catch {
        case exception: Exception => println("Wrong Cast")
      }
    }
    
    name match {
      case "name" => {
        answers.update(0, Option(control.asInstanceOf[javafx.scene.control.TextField].text.value + e.asInstanceOf[KeyEvent].character))
      }
      case "gender" => {
        answers.update(1, Option(answer))
      }
      case "age" => {
        answers.update(2, Option(control.asInstanceOf[javafx.scene.control.Spinner[Int]].value.value.toString))
      }
      case "internetAge" => {
        answers.update(3, Option(control.asInstanceOf[javafx.scene.control.Spinner[Int]].value.value.toString))
      }
      case "averageHour" => {
        answers.update(4, Option(control.asInstanceOf[javafx.scene.control.Spinner[Int]].value.value.toString))
      }
      /** question6 */
      case "homeUsage" => {
        val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
        convControl.parent.value.asInstanceOf[javafx.scene.layout.HBox].children.foreach {
          case a:javafx.scene.layout.VBox => a.disable = !a.disable.value;
          case _ =>
        }
        location.update(0, Option(convControl.selected.value))
        if (!convControl.selected.value) {
          answers.update(5, None)
        }
      }
      case "hotspotUsage" => {
        val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
        convControl.parent.value.asInstanceOf[javafx.scene.layout.HBox].children.foreach {
          case a:javafx.scene.layout.VBox => a.disable = !a.disable.value
          case _ =>
        }
        location.update(1, Option(convControl.selected.value))
        if (!convControl.selected.value) {
          answers.update(6, None)
        }
      }
      case "homeHours" => {
        answers.update(5, Option(control.asInstanceOf[javafx.scene.control.Spinner[Int]].value.value.toString))
      }
      case "outsideHours" => {
        answers.update(6, Option(control.asInstanceOf[javafx.scene.control.Spinner[Int]].value.value.toString))
      }
      case "otherPlaces" => {
        if (!controlId.contains("complex")) {
          val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
          convControl.parent.value.asInstanceOf[javafx.scene.layout.HBox].children.foreach {
            case a:javafx.scene.layout.VBox => a.disable = !a.disable.value
            case _ =>
          }
          if (!convControl.selected.value) {
            answers.update(8, None)
            answers.update(7, None)
          }
        } else {
          val convControl = control.asInstanceOf[javafx.scene.control.TextField]
          answers.update(7, Option(convControl.text.value + e.asInstanceOf[KeyEvent].character))
        }
        
      }
      case "otherPlacesHours" => {
        answers.update(8, Option(control.asInstanceOf[javafx.scene.control.Spinner[Int]].value.value.toString))
      }
    	/** question7 */
      case "doingOntNet" => {
        val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
        doingOnNet.update(splitId(1), Option(convControl.selected.value))
      }
      case "otherWork" => {
        val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
        convControl.parent.value.asInstanceOf[javafx.scene.layout.HBox].children.foreach {
          case a:javafx.scene.layout.VBox => a.disable = !a.disable.value
          case _ =>
        }
      }
      case "otherDoing" => {
        answers.update(9, Option(control.asInstanceOf[javafx.scene.control.TextField].text.value + e.asInstanceOf[KeyEvent].character))
      }
    	/** question8 */
      case "onlineDevices" => {
        val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
        onlineDevice.update(splitId(1), Option(convControl.selected.value))
      }
      case "otherDevice" => {
        val convControl = control.asInstanceOf[javafx.scene.control.CheckBox]
        convControl.parent.value.asInstanceOf[javafx.scene.layout.HBox].children.foreach {
          case a:javafx.scene.layout.VBox => a.disable = !a.disable.value
          case _ =>
        }
      }
      case "otherDevices" => {
        answers.update(10, Option(control.asInstanceOf[javafx.scene.control.TextField].text.value + e.asInstanceOf[KeyEvent].character))
      }
    	/** question9 */
      case "characterizeQuestion" => {
        val convControl = control.asInstanceOf[javafx.scene.control.RadioButton]
        characterize.update(splitId(1), splitId(2).toDouble.toInt)
      }
    	/** question10 */
      case "applyQuestion" => {
        val convControl = control.asInstanceOf[javafx.scene.control.RadioButton]
        apply.update(splitId(1), splitId(2).toDouble.toInt)
      }
    	/** question11 */
      case "describeYouQuestion" => {
        val convControl = control.asInstanceOf[javafx.scene.control.RadioButton]
        describe.update(splitId(1), splitId(2).toDouble.toInt)
      }
      case _ => {
        println(controlId)
      }
    }
  }
  
  def menuItemOnFired(e: ActionEvent) {
    var menuItem = new MenuItem(e.source.asInstanceOf[javafx.scene.control.MenuItem])
    menuItem.id.value match {
      case "NewAnswerMenu" => {
        SurveyDetailsPane.visible = true
        ListingsPane.visible = false
        SaveMenuButton.disable = false
        SurveyDetailsAnchorPane.children+=vBoxAndId._1
      }
      case "CloseMenuButton" => {
        openStage.close
      }
      case "AboutMenuButton" => {
        openedStage.scene.value.root = aboutRoot
        openedStage.title.value = "About this application"
        if (!openedStage.showing.value) {
          openedStage.show
        }
        openedStage.toFront
      }
      case "HowToMenuButton" => {
        openedStage.scene.value.root = helpRoot
        openedStage.title.value = "How To"
        if (!openedStage.showing.value) {
          openedStage.show
        }
        openedStage.toFront
      }
      case "MenuChart" => {
        if (!chartStage.showing.value) {
          chartStage.show
        }
        chartControl.startUp(chartStage, answerArrayBuffer.toArray)
      }
    }
  }
  
  def listViewClicked(e: MouseEvent) {
    val listCell = new TextFieldListCell(e.source.asInstanceOf[javafx.scene.control.cell.TextFieldListCell[String]])
    if(e.button.equals(MouseButton.Primary) && listCell.text.value != null && !listCell.text.value.equals(tempFileName)){
      tempFileName = listCell.text.value
      resultObj = Utilities.loadSerializedObject[SurveyAnswer[ExcessiveInternetSurvey]](new File("projects_answers").toString, listCell.text.value).get
      SurveyDetails.children+=engineObj.generateRootLayoutSurveyWithAnswer(surveystuff, surveyControlsHandler, resultObj.survey)._1
    }
  }
  
  def loginError {
    new Alert (Alert.AlertType.Error) {
      title = "Fail Login"
      headerText = "Authentication to server failed"
      contentText = "Re enter Login details"
    }.showAndWait
    prototype.Prototype.loginController.passStage(loginStage)
    loginStage.showAndWait
  }
  
  def reMapValues(answer: ExcessiveInternetSurvey) {
    location.update(0, answer.locations.homeUsage)
    location.update(1, answer.locations.hotspotUsage)
    doingOnNet.update("searchInfo", answer.doingOntNet.searchInfo)
    doingOnNet.update("readNews", answer.doingOntNet.readNews)
    doingOnNet.update("writeForum", answer.doingOntNet.writeForum)
    doingOnNet.update("useSocial", answer.doingOntNet.useSocial)
    doingOnNet.update("playgame", answer.doingOntNet.playgame)
    onlineDevice.update("pc", answer.onlineDevices.pc)
    onlineDevice.update("laptop", answer.onlineDevices.laptop)
    onlineDevice.update("tablet", answer.onlineDevices.tablet)
    onlineDevice.update("mobile", answer.onlineDevices.mobile)
    val chara = answer.characterizeQuestion
    chara.getClass.getDeclaredFields.foreach{
      case e: Field => {
        e.setAccessible(true)
        characterize.update(e.getName, e.get(chara).asInstanceOf[Int])
      }
    }
    val app = answer.applyQuestion
    app.getClass.getDeclaredFields.foreach{
      case e: Field => {
        e.setAccessible(true)
        apply.update(e.getName, e.get(app).asInstanceOf[Int])
      }
    }
    val des = answer.describeYouQuestion
    des.getClass.getDeclaredFields.foreach{
      case e: Field => {
        e.setAccessible(true)
        describe.update(e.getName, e.get(des).asInstanceOf[Int])
      }
    }
    var runner = 1
    answer.getClass.getDeclaredFields.foreach {
      case f: Field => {
        println(runner)
        f.setAccessible(true)
        try {
          answers.update(runner, Some(f.get(answer).asInstanceOf[String]))
          runner+=1
        } catch {
           case e: ClassCastException => {
             try {
               answers.update(runner, Some(String.valueOf(f.get(answer).asInstanceOf[Int])))
               runner+=1
             } catch {
               case e: ClassCastException => {
                 try {
                   val something = f.get(answer).asInstanceOf[Option[Any]]
                   if (something.get.isInstanceOf[String]) {
                     answers.update(runner, Some(something.get.asInstanceOf[String]))
                     runner+=1
                   } else if (something.get.isInstanceOf[Int]) {
                      answers.update(runner, Some(something.get.asInstanceOf[Int].toString))
                      println(runner, "OPTION INT", answers(runner))
                      runner+=1
                   } else {
                     println("WTH")
                   }
                 } catch {
                   case e: ClassCastException => {
                     println("yaya I know")
                   }
                 }
                 
               }
             }
           }
        }
      }
    }
  }
  
  def transferDataToCase: SurveyAnswer[ExcessiveInternetSurvey] = {
    val doingOnNetKeys = doingOnNet.keysIterator.toArray
    val onlineDeviceKeys = onlineDevice.keysIterator.toArray
    val characterizeKeys = characterize.keysIterator.toArray
    val applyKeys = apply.keysIterator.toArray
    val describeKeys = describe.keysIterator.toArray
    
    val surveyAns = ExcessiveInternetSurvey(
	    gender = answers(1).get,
	    age = answers(2).get.toInt,
	    internetAge = answers(3).get.toInt,
	    averageHour = answers(4).get.toInt,
	    locations = LocationUsage(
	        location(0),
	        location(1)
      ),
	    homeHours = Some(answers(5).get.toInt),
    	outsideHours = Some(answers(6).get.toInt),
    	otherPlaces = answers(7),
    	otherPlacesHours = Some(answers(8).get.toInt),
    	/** question7 */
    	doingOntNet = DoingOnNet(
    	    doingOnNet.get(doingOnNetKeys(0)).get,
    	    doingOnNet.get(doingOnNetKeys(1)).get,
    	    doingOnNet.get(doingOnNetKeys(2)).get,
    	    doingOnNet.get(doingOnNetKeys(3)).get,
    	    doingOnNet.get(doingOnNetKeys(4)).get
	    ),
    	otherDoing = Some(answers(9).get),
    	/** question8 */
    	onlineDevices = OnlineDevice(
    	    onlineDevice.get(onlineDeviceKeys(0)).get,
    	    onlineDevice.get(onlineDeviceKeys(1)).get,
    	    onlineDevice.get(onlineDeviceKeys(2)).get,
    	    onlineDevice.get(onlineDeviceKeys(3)).get
	    ),
    	otherDevices = Some(answers(10).get),
    	/** question9 */
    	characterizeQuestion = CharacterizeQuestion(
    	    characterize.get(characterizeKeys(0)).get,
    	    characterize.get(characterizeKeys(1)).get,
    	    characterize.get(characterizeKeys(2)).get,
    	    characterize.get(characterizeKeys(3)).get,
    	    characterize.get(characterizeKeys(4)).get,
    	    characterize.get(characterizeKeys(5)).get,
    	    characterize.get(characterizeKeys(6)).get,
    	    characterize.get(characterizeKeys(7)).get,
    	    characterize.get(characterizeKeys(8)).get,
    	    characterize.get(characterizeKeys(9)).get,
    	    characterize.get(characterizeKeys(10)).get,
    	    characterize.get(characterizeKeys(11)).get,
    	    characterize.get(characterizeKeys(12)).get,
    	    characterize.get(characterizeKeys(13)).get,
    	    characterize.get(characterizeKeys(14)).get,
    	    characterize.get(characterizeKeys(15)).get,
    	    characterize.get(characterizeKeys(16)).get,
    	    characterize.get(characterizeKeys(17)).get
	    ),
    	/** question10 */
    	applyQuestion = ApplyQuestion(
    	    apply.get(applyKeys(0)).get,
    	    apply.get(applyKeys(1)).get,
    	    apply.get(applyKeys(2)).get,
    	    apply.get(applyKeys(3)).get,
    	    apply.get(applyKeys(4)).get,
    	    apply.get(applyKeys(5)).get,
    	    apply.get(applyKeys(6)).get,
    	    apply.get(applyKeys(7)).get,
    	    apply.get(applyKeys(8)).get,
    	    apply.get(applyKeys(9)).get,
    	    apply.get(applyKeys(10)).get,
    	    apply.get(applyKeys(11)).get,
    	    apply.get(applyKeys(12)).get,
    	    apply.get(applyKeys(13)).get,
    	    apply.get(applyKeys(14)).get,
    	    apply.get(applyKeys(15)).get,
    	    apply.get(applyKeys(16)).get,
    	    apply.get(applyKeys(17)).get,
    	    apply.get(applyKeys(18)).get,
    	    apply.get(applyKeys(19)).get,
    	    apply.get(applyKeys(20)).get
	    ),
    	/** question11 */
    	describeYouQuestion = DescribeYouQuestion(
    	    describe.get(describeKeys(0)).get,
    	    describe.get(describeKeys(1)).get,
    	    describe.get(describeKeys(2)).get,
    	    describe.get(describeKeys(3)).get,
    	    describe.get(describeKeys(4)).get,
    	    describe.get(describeKeys(5)).get,
    	    describe.get(describeKeys(6)).get,
    	    describe.get(describeKeys(7)).get,
    	    describe.get(describeKeys(8)).get,
    	    describe.get(describeKeys(9)).get,
    	    describe.get(describeKeys(10)).get,
    	    describe.get(describeKeys(11)).get,
    	    describe.get(describeKeys(12)).get,
    	    describe.get(describeKeys(13)).get,
    	    describe.get(describeKeys(14)).get,
    	    describe.get(describeKeys(15)).get,
    	    describe.get(describeKeys(16)).get,
    	    describe.get(describeKeys(17)).get,
    	    describe.get(describeKeys(18)).get,
    	    describe.get(describeKeys(19)).get
	    )
  	)
  	
  	if (usingFileName.isEmpty) {
      SurveyAnswer[ExcessiveInternetSurvey](
        surveyId = "ExcessiveInternetSurvey",
        email = clientDetails.client.username,
        time = new DateTime(),
        survey = surveyAns
      )
  	} else {
  	  SurveyAnswer[ExcessiveInternetSurvey](
        surveyId = "ExcessiveInternetSurvey",
        email = clientDetails.client.username,
        time = resultObj.time,
        survey = surveyAns
      )
  	}
    
  }
  
  def buttonOnFired(e: ActionEvent){
    var button = new Button(e.source.asInstanceOf[javafx.scene.control.Button])
    var buttonId = button.id.value
    buttonId match{
      case "SaveButton" => {
        import spray.json._
        
        import org.joda.time.DateTime
        
        val answerObj = transferDataToCase
        val jsonObject = answerObj.toJson
        
        var filename = ""
        val dateTimeSplit = (new DateTime).toString.split(":")
        if (usingFileName.isEmpty) {
          val splitAgain = dateTimeSplit(0).split("T")
          filename = "saved-" + splitAgain(0) + "_" + splitAgain(1) + dateTimeSplit(1)
          val status = Utilities.saveLocally(saved.toString + File.separator, filename, jsonObject.toString)
          val statustwo = Utilities.saveLocally(saved.toString + File.separator, filename, answerObj)
        } else {
          filename = usingFileName.split("_")(0) + "_" + usingFileName.split("_")(1) + "_r" + dateTimeSplit(1);
          println(tempFileName)
          val status = Utilities.saveLocally(saved.toString + File.separator, filename, jsonObject.toString)
          val statustwo = Utilities.saveLocally(saved.toString + File.separator, filename, answerObj)
        }
        resultSubmitter.addSubmission(filename)
        initializeAllResultArray
        initialize
      }
      case "AddNewButton" => {
        SurveyDetailsPane.visible = true
        ListingsPane.visible = false
        SaveMenuButton.disable = false
        SurveyDetailsAnchorPane.children+=vBoxAndId._1
      }
      case "EditButton" => {
        if (tempFileName.contains("SUBMITTED")) {
          new Alert (Alert.AlertType.Error) {
            title = "Edit Error"
            headerText = "Unable to edit"
            contentText = "Edit failed\nReasons: file has already been submitted"
          }.showAndWait
        } else {
          usingFileName = tempFileName
          reMapValues(resultObj.survey)
          SurveyDetailsAnchorPane.children.clear
          SurveyDetails.children(0).disable = false
          SurveyDetailsAnchorPane.children+=SurveyDetails.children(0)
          SurveyDetailsPane.visible = true
          ListingsPane.visible = false
          SaveMenuButton.disable = false
        }
      }
      case _ => 
    }
  }
}