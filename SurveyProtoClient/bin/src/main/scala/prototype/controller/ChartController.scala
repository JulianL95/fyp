package prototype.controller

import scalafxml.core.macros.sfxml

import scalafx.scene.chart.LineChart
import scalafx.scene.control.MenuItem
import scalafx.event.ActionEvent
import scalafx.stage.Stage
import scalafx.scene.chart.XYChart._
import scalafx.scene.chart.XYChart
import scalafx.beans.property.ObjectProperty
import scalafx.Includes._

import api.models.questionaireanswermodel._
import api.models._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

@sfxml
class ChartController(
    DASSA: LineChart[String, Int],
    DASSD: LineChart[String, Int],
    DASSS: LineChart[String, Int],
    PIUQ: LineChart[String, Int],
    SIAS: LineChart[String, Int],
    MenuDASSA: MenuItem,
    MenuDASSD: MenuItem,
    MenuDASSS: MenuItem,
    MenuPIUQ: MenuItem,
    MenuSIAS: MenuItem,
    MenuClose: MenuItem
){
  
  var stage: Stage = null
  var data: Array[SurveyAnswer[ExcessiveInternetSurvey]] = null
  
  var piuqSeries: XYChart.Series[String, Int] = null
  var dassaSeries: XYChart.Series[String, Int] = null
  var dassdSeries: XYChart.Series[String, Int] = null
  var dasssSeries: XYChart.Series[String, Int] = null
  var siasSeries: XYChart.Series[String, Int] = null
  
  def menuHandler(e: ActionEvent) {
    var menuItem = new MenuItem(e.source.asInstanceOf[javafx.scene.control.MenuItem])
    menuItem.id.value match {
      case "MenuPIUQ" => {
        DASSA.visible = false
        DASSS.visible = false
        DASSD.visible = false
        PIUQ.visible = true
        SIAS.visible = false
      }
      case "MenuSIAS" => {
        DASSA.visible = false
        DASSS.visible = false
        DASSD.visible = false
        PIUQ.visible = false
        SIAS.visible = true
      }
      case "MenuDASSA" => {
        DASSA.visible = true
        DASSS.visible = false
        DASSD.visible = false
        PIUQ.visible = false
        SIAS.visible = false
      }
      case "MenuDASSS" => {
        DASSA.visible = false
        DASSS.visible = true
        DASSD.visible = false
        PIUQ.visible = false
        SIAS.visible = false
      }
      case "MenuDASSD" => {
        DASSA.visible = false
        DASSS.visible = false
        DASSD.visible = true
        PIUQ.visible = false
        SIAS.visible = false
      }
      case "MenuClose" => {
        stage.close
      }
      case _ =>
    }
  }
  
  def startUp(s: Stage, allStoredData: Array[SurveyAnswer[ExcessiveInternetSurvey]]) {
    stage = s
    data = allStoredData
    splitData
  }
  
  def splitData {
    PIUQ.data.value.clear
    DASSS.data.value.clear
    DASSD.data.value.clear
    DASSA.data.value.clear
    SIAS.data.value.clear
    piuqSeries = new XYChart.Series[String, Int] {
      name = "PIUQ"
    }
    dassaSeries = new XYChart.Series[String, Int] {
      name = "DASSA"
    }
    dassdSeries = new XYChart.Series[String, Int] {
      name = "DASSD"
    }
    dasssSeries = new XYChart.Series[String, Int] {
      name = "DASSS"
    }
    siasSeries = new XYChart.Series[String, Int] {
      name = "SIAS"
    }
    println(data.length)
    data.foreach(
      ans => {
        val dateTime = ans.time.toString(DateTimeFormat.forPattern("dd MM YY, HH:mm"))
        val piuq = ans.survey.PIUQ.toInt
        println(piuq)
        piuqSeries.data.value+=XYChart.Data[String, Int](dateTime, piuq)
        val dasss = ans.survey.DASSStress.toInt
        println(ans.survey.DASSStress)
        dasssSeries.data.value+=XYChart.Data[String, Int](dateTime, dasss)
        val dassd = ans.survey.DASSDepresion.toInt
        println(ans.survey.DASSDepresion)
        dassdSeries.data.value+=XYChart.Data[String, Int](dateTime, dassd)
        val sias = ans.survey.SIAS.toInt
        println(ans.survey.SIAS)
        siasSeries.data.value+=XYChart.Data[String, Int](dateTime, sias)
        val dassa = ans.survey.DASSAnxiety.toInt
        println(ans.survey.DASSAnxiety)
        dassaSeries.data.value+=XYChart.Data[String, Int](dateTime, dassa)
      }
    )
    PIUQ.data.value+=piuqSeries
    DASSS.data.value+=dasssSeries
    DASSD.data.value+=dassdSeries
    DASSA.data.value+=dassaSeries
    SIAS.data.value+=siasSeries
  }
  
}