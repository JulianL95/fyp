package prototype

import scalafx.application.{ JFXApp, Platform }
import scalafxml.core.{NoDependencyResolver, FXMLLoader}
import scalafx.scene.control.Alert
import scalafx.stage.Stage
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.Includes._

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.util.{ Success, Failure }
import scala.concurrent.ExecutionContext.Implicits.global

import spray.json._

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import java.io.File
import java.nio.charset.Charset
import java.lang.Thread

import api.utils.Utilities
import api.linkage.{ Communication, Connection }
import prototype.result.ResultSubmitter
import api.utils.ImplicitUtilities._
import controller.{ MainController, LoginController }
import api.models.authdetailsmodel._
import api.models.questionaireanswermodel._
import api.models.questionaireanswermodel.AnswerJsonProtocol._

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ HttpRequest, HttpMethods, StatusCodes, HttpResponse }
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.model._
import MediaTypes._
import HttpCharsets._

object Prototype extends JFXApp{
  System.setProperty("java.net.preferIPv4Stack" , "true");
  val loader = new FXMLLoader(getClass.getClassLoader.getResource("prototype/view/MainInterface.fxml"), NoDependencyResolver )
  loader.load
  val rootUI = loader.getRoot[javafx.scene.layout.BorderPane]
  val loginLoader = new FXMLLoader(getClass.getClassLoader.getResource("prototype/view/Login.fxml"), NoDependencyResolver )
  loginLoader.load
  val loginRoot = loginLoader.getRoot[javafx.scene.layout.AnchorPane]
  println(loginRoot)
  
  val surveyController = loader.getController[MainController#Controller]
  val loginController = loginLoader.getController[LoginController#Controller]
  
  val connection = new Connection("cwc.hep88.com", 2000)
  var relogBoolean = false

  val clientFolder = new File("client")
  clientFolder.mkdir
  var first = true
  val clientFile = new File("client" + File.separator + "client.srl")
  if (clientFile.exists) {
    first = false
  }
  val thread = Thread.currentThread
  println(thread.getName)
  
  var sessionDetails: ClientSessionDetails = null
  var submissionDetails: SubmissionDetailsRetriever = null
  var submitter: ResultSubmitter = null
  
  val communication = new Communication(connection)
  
  var clientObj: Client = null
  if (first) {
    stage = new PrimaryStage {
      resizable = false
      scene = new Scene{
        root = loginRoot
      }
      title = "Login"
    }
  } else {
    stage = new PrimaryStage {
      resizable = false
      scene = new Scene{
        root = rootUI
      }
      height = 720
      width = 1280
      title = "Offline Survey Framework Prototype"
    }
    clientObj = Utilities.loadSerializedObject[Client](clientFolder.toString, "client").get
    sessionDetails = new ClientSessionDetails(connection, clientObj)
    sessionDetails.getInitialToken( 
        initialSessionTokenRetriever
    )
    
    submissionDetails = new SubmissionDetailsRetriever(connection, sessionDetails)
    submissionDetails.getInitialToken(
        initialSubmissionTokenRetriever
    )
    submissionDetails.scheduleRefreshToken(43200, initialSubmissionTokenRetriever)
    
    submitter = ResultSubmitter(submissionDetails, connection)
    
    surveyController.startUp(stage, first, sessionDetails, submitter, loginRoot)
  }
  
  def reRunSubmissionInitialTokenRetriever {
    println(clientObj.usernameTouched, clientObj.password)
    submissionDetails.getInitialToken(
        initialSubmissionTokenRetriever
    )
  }
  
  def changeScene(username: String, password: String) {
    println(username.mkString("\n"))
    println(password.mkString("\n"))
    if (!relogBoolean) {
      clientObj = new Client(username, password)
      Utilities.saveLocally(clientFolder.toString, "client", clientObj)
      stage.scene.value.root = rootUI
      stage.width = 1280
      stage.height = 720
      stage.title.value = "Offline Survey Framework Prototype"
      
      sessionDetails = new ClientSessionDetails(connection, clientObj)
      sessionDetails.getInitialToken( 
          initialSessionTokenRetriever
      )
      
      submissionDetails = new SubmissionDetailsRetriever(connection, sessionDetails)
      submissionDetails.getInitialToken(
          initialSubmissionTokenRetriever
      )
      submissionDetails.scheduleRefreshToken(43200, initialSubmissionTokenRetriever)
      
      submitter = ResultSubmitter(submissionDetails, connection)
      
      surveyController.startUp(stage, first, sessionDetails, submitter, loginRoot)
    } else {
      clientObj = new Client(username, password)
      sessionDetails.client = clientObj
      Utilities.saveLocally(clientFolder.toString, "client", clientObj)
      reRunSubmissionInitialTokenRetriever
      relogBoolean = false
    }
    
  }
  
  def actorSystem = system
  
  def initialSubmissionTokenRetriever(token: SessionToken): Future[SubmissionAuthToken] = {
    communication.returnResultFromHTTPRequest[SubmissionAuthToken](
        (respond: HttpResponse) => {
          respond.status match {
            case StatusCodes.OK => {
			        println("success")
              val entity = respond.entity.toStrict(5.seconds).flatMap[SubmissionAuthToken](
                htmlString => {
                  val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                  Future{
                    val tokenObj = htmlString.data.decodeString(Charset.defaultCharset)
                    val splitToken = tokenObj.split(",")
                    val token = splitToken(0).split(":")
                    val tokenVal = token(1).substring(1, token(1).length-1)
                    val expiresOn = splitToken(1).split("\"")
                    val expiresOnVal = expiresOn(3)
                    SubmissionAuthToken(tokenVal, Option(DateTime.parse(expiresOnVal, DateTimeFormat.forPattern(dateFormat))))
                  }
                }
              )
              entity
            }
            case _ => {
              println("Status code error")
              val entity = respond.entity.toStrict(5.seconds).flatMap[SubmissionAuthToken](
                htmlString => {
                  val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                  val errorString = htmlString.data.decodeString(Charset.defaultCharset)
                  println(errorString)
                  val errorMsg = errorString.split(":")(1)
                  if (errorMsg.contains("Invalid credentials")) {
                    relogBoolean = true
                    Platform.runLater {
                      () =>
                        surveyController.loginError
                    }
                  } else {
                    println("UNKNOWN REASON")
                    println(errorString.split(":").mkString("\n"))
                  }
                  Future{SubmissionAuthToken("", None)}
                }
              )
              entity
            }
          }
        },
        "auth/api/authenticate/userpass",
        HttpMethods.POST,
        HttpEntity(ContentType(MediaTypes.`application/x-www-form-urlencoded`, `UTF-8`), "csrfToken="+sessionDetails.token.token+"&username="+sessionDetails.client.usernameTouched+"&password="+sessionDetails.client.password)
    )
  }
  
  def initialSessionTokenRetriever(client: Client): Future[SessionToken] = {
    communication.returnResultFromHTTPRequest[SessionToken](
        (respond: HttpResponse) => {
          respond.status match {
            case StatusCodes.OK => {
              val entity = respond.entity.toStrict(5.seconds).flatMap[SessionToken](
                htmlString => {
                  Future{
                    val tokenObj = htmlString.data.decodeString(Charset.defaultCharset)
                    val splitToken = tokenObj.split(",")
                    val token = splitToken(1).split("\"")(3)
                    SessionToken(token, None)
                  }
                }
              )
              entity
            }
            case _ => {
              println("Status code error")
              Future{SessionToken("", None)}
            }
          }
        },
        "csrf/getTokenJson",
        HttpMethods.GET
    )
  }
  
  override def stopApp{
    if (submitter != null && sessionDetails != null && submissionDetails != null) {
      submitter.endScheduler
      submitter.saveProgress
      sessionDetails.stopScheduler
      submissionDetails.stopScheduler
    }
    connection.endScheduler
    api.utils.ImplicitUtilities.killAll
  }
}

