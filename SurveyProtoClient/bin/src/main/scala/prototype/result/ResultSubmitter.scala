package prototype.result

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import scalafx.application.Platform

import java.util.concurrent.ConcurrentLinkedQueue
import java.io.File
import java.net.InetAddress
import java.nio.charset.Charset

import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.HttpCharsets._
import akka.http.scaladsl.model.MediaTypes._

import spray.json._

import api.models.authdetailsmodel._
import api.utils.ImplicitUtilities._
import api.utils.Utilities
import api.models.SurveyAnswer
import api.models.questionaireanswermodel._
import api.models.questionaireanswermodel.AnswerJsonProtocol._
import api.linkage._

/** Result Submitter class submits result using the Communication companion object. Singleton class as only 1 instance of the ResultSubmitter is allowed as the directory the class is using may suffer race condition if there are multiple instances of this class
 *
 * @param submissionDetails required because the in order to submit the submission token is needed
 * @param connection needed as the task requires internet connection
 */
class ResultSubmitter private(submissionDetails: SubmissionDetailsRetriever, connection: Connection){
  private val submitArray = new ConcurrentLinkedQueue[String]
  private val actor = system.actorOf(Props(classOf[ResultSubmitterActor], this), "submitterActor")
  private val scheduler = system.scheduler.schedule(Duration.Zero, Duration(5, SECONDS), actor, "RUN")
  private val communication = new Communication(connection)
  
  private val submissionFolder = new File("submissions")
  submissionFolder.mkdir
  private val submissionFile = new File("submissions\\to_submit.srl")
  if (submissionFile.exists) {
    val toSubmit = Utilities.loadSerializedObject[ConcurrentLinkedQueue[String]](submissionFolder.toString, "to_submit")
    if (toSubmit != None) {
      val toSubmitObj = toSubmit.get
      for (x <- 0 to toSubmitObj.size-1) {
        submitArray.add(toSubmitObj.poll)
      }
    }
  }
  
  /** Save progress method serialize the array containing the submission file names
   *
   */
  def saveProgress {
    Utilities.saveLocally[ConcurrentLinkedQueue[String]](submissionFolder.toString, "to_submit", submitArray)
  }
  
  /** Queue the file based on the filename for submission
   *
   * @param filename name of the file that needs to be submitted
   */
  def addSubmission(filename: String) {
    submitArray.add(filename)
  }
  
  /** End the scheduler of this class's actor
   *
   */
  def endScheduler {
    scheduler.cancel
  }
  
  private class ResultSubmitterActor extends Actor {
    override def receive = {
      case "RUN" => {
        println("Running result submission actor")
        if (!submitArray.isEmpty && !submissionDetails.token.token.isEmpty) {
          var answer = Utilities.loadSerializedObject[SurveyAnswer[ExcessiveInternetSurvey]](new File("projects_answers").toString, submitArray.peek).get
          println(answer.email)
          println(submissionDetails.sessionDetails.client.username)
          if (!answer.email.equals(submissionDetails.sessionDetails.client.username)) {
            println("yep")
            answer = SurveyAnswer[ExcessiveInternetSurvey](
              surveyId = answer.surveyId,
            	email = submissionDetails.sessionDetails.client.username,
            	time = answer.time,
            	survey = answer.survey
            )
          }
          new File("projects_answers"+File.separator+submitArray.peek+".srl").delete
          new File("projects_answers"+File.separator+submitArray.peek+".json").delete
          Utilities.saveLocally[SurveyAnswer[ExcessiveInternetSurvey]](new File("projects_answers").toString, submitArray.peek, answer)
          Utilities.saveLocally(new File("projects_answers").toString, submitArray.peek, answer.toJson.toString)
          connection.registerTaskOnConnected(
            (ip: InetAddress) => {
              val res = communication.returnResultFromHTTPRequest[Unit](
                (respond: HttpResponse) => Future{
                  respond.status match {
                    case StatusCodes.OK => {
                      val oriFileName = submitArray.poll
                      val pathFile = "projects_answers" + File.separator + oriFileName + ".srl"
                      val newName = "projects_answers" + File.separator + oriFileName + "_SUBMITTED" + ".srl"
                      new File(pathFile).renameTo(new File(newName))
                      Platform.runLater(prototype.Prototype.surveyController.listHandler(false))
                    }
                    case StatusCodes.BadRequest => {
                      val entity = respond.entity.toStrict(5.seconds).flatMap[Unit](
                        htmlString => {
                          Future{
                            val objectThingy = htmlString.data.decodeString(Charset.defaultCharset)
                            if (objectThingy.contains("duplicate data")) {
                              val oriFileName = submitArray.poll
                              Platform.runLater(prototype.Prototype.surveyController.listHandler(true, oriFileName))
                            }
                          }
                        }
                      )
                    }
                    case _ => {
                      println("Status code error")
                      println(respond.status)
                      println(respond)
                      val entity = respond.entity.toStrict(5.seconds).flatMap[Unit](
                        htmlString => {
                          Future{
                            val objectThingy = htmlString.data.decodeString(Charset.defaultCharset)
                          }
                        }
                      )
                    }
                  }
                },
                "survey/api/save",
                HttpMethods.POST,
                HttpEntity(ContentType(MediaTypes.`application/json`) , answer.toJson.toString),
                RawHeader(name = "X-Auth-Token", value = submissionDetails.token.token)
              )
            }
          )
        }
      }
    }
  }
}

object ResultSubmitter {
  private var submitter: ResultSubmitter = null
  
  /*
   * Singleton
   */
  def apply(loginDetails: SubmissionDetailsRetriever, link: Connection): ResultSubmitter = {
    if (submitter == null) {
      submitter = new ResultSubmitter(loginDetails, link)
    }
    submitter
  }
}