package api.linkage

import akka.util.Timeout
import akka.actor.{ Actor, ActorRef, Cancellable, Props }

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.collection.mutable.{ArrayBuffer, ListMap}

import java.net.{ InetAddress, UnknownHostException }
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicBoolean

import api.utils.ImplicitUtilities._

/** Automatically rechecks IP reachability every timeoutValue multiply by the amount of ip addresses this hostname has. If the hostname have not resolve to IP address yet the listmap will be empty
 * 
 * @constructor hostname the hostname of which this connection class needs to connect to, timeoutValue the default time to wait for every process that requires timeout
 */
class Connection (val hostname: String, val timeoutValue: Int) {
	implicit val timeout = Timeout(Duration(timeoutValue, "seconds"))
	private val reachableIP = ListMap[InetAddress, Option[Boolean]]()
	private val addresses = ArrayBuffer[InetAddress]()
	private val fnArr = new ConcurrentLinkedQueue[(InetAddress) => Unit]()
	private val actor = system.actorOf(Props(classOf[ConnectionActor], this, reachableIP, addresses, timeoutValue, fnArr), "ConnectionActor")
	private var scheduleCanceler: Cancellable = null
	private var end = new AtomicBoolean(false)
	
	actor ! ConnectionScheduler(
		() => initializeOne, 1 )
		
	private def initializeOne = {
		try {
	    addresses ++= InetAddress.getAllByName(hostname)
	    scheduleCanceler.cancel
	    addresses.foreach{
	      address => {
	        reachableIP+=(address -> None)
	      }
	    }
	    actor ! ConnectionScheduler(() => initializeTwo, timeoutValue*addresses.length)
	  } catch {
	    case e: UnknownHostException => {
	    }
	  }
	}

	private def initializeTwo = {
		if (!reachableIP.isEmpty){
			system.actorOf(Props(classOf[ConnectionActor], this, reachableIP, addresses, timeoutValue, fnArr), "unstoredConnectionActor") ! "CheckAllAddress"
		}
	}

	/** This will return false if all the IP addresses related to the hostname given
   * have not yet been resolved
   * This will also return false if none of the IP from the hostname can be reached
   * 
   * @return the boolean value if it can be reached
   */
	def canBeReached: Boolean = {
		if (reachableIP.isEmpty || addresses.isEmpty) {
			return false
		} else {
			reachableIP.keysIterator.foreach(
	      ip => {
	        if (reachableIP(ip) != None) {
	          return true
	        }
	      }
	    )
	    false
		}
	}
	
	def endScheduler {
	  end.set(!end.get)
	  scheduleCanceler.cancel
	}

	/** The return value is an InetAddress and a boolean which states that whether or not it is reachable. If the address is not yet resolved the ListMap will be empty, None value for corresponding InetAddress means that the InetAddress cannot be reached
   * 
   * @return A listmap which contains the the reachability of the inetaddresses, key being the InetAddress and the Option[Boolean] is the reachability.
   */
  def ipAddressReachability: ListMap[InetAddress, Option[Boolean]] = {
    reachableIP
  }
  
  /** Register a function when one of the InetAddress reachability becomes true
   * 
   * @param task the function to run when there is Internet access
   */
  def registerTaskOnConnected(task:(InetAddress) => Unit) {
    fnArr.add(task)
  }
	

  private case class ConnectionScheduler(task:() => Unit, intervals: Int)
  private case class RunScheduledTask(task:() => Unit)
  private case class IsReachable(allAddresses: ArrayBuffer[InetAddress])


  private class ConnectionActor(
      ipArray: ListMap[InetAddress, Option[Boolean]],
      allAddresses: ArrayBuffer[InetAddress],
      timeout: Int,
      fnArray: ConcurrentLinkedQueue[(InetAddress) => Unit]) extends Actor {
    override def receive = {
      case "CheckAllAddress" => {
        while(!end.get) {
        	allAddresses.foreach{
        		case address: InetAddress => {
				var reachable = false
        		  try{
				    reachable = address.isReachable(timeout)
				  } catch {
					case e: Exception => 
				  }
				  if (reachable) {
				    ipArray.update(address, Some(reachable))
				    if (!fnArray.isEmpty) {
					  val functionObj = fnArray.poll
					  try {
					    functionObj(address)
					  } catch {
					    case e:akka.stream.StreamTcpException => fnArray.add(functionObj)
					  }
				    }
				  }
        		}
        	}
        }
      }
      case RunScheduledTask(task) => {
      	task()
      }
      case ConnectionScheduler(task, intervals) => {
        scheduleCanceler = system.scheduler.schedule(Duration.Zero, Duration(intervals, SECONDS), self, RunScheduledTask(task))
      }
      case "Stop" => {
  //      context.stop(self)
      }
      
    }
  }
}