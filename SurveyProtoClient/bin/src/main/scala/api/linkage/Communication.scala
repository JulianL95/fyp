package api.linkage

/*
 * Akka HTTP is used in this case because utilizing akka tcp protocol by using socket is hard to maintain as 
 * the problem for this project is mainly for the users which are having connection problem
 * whether it is slow connection or unstable connection.
 */

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Failure, Success }

import java.net.InetAddress

import api.utils.ImplicitUtilities._
import api.models.authdetailsmodel._

/** HTTP communication with server, package HTTP request
 * 
 */
class Communication (connection: Connection){
  /** The only method of this class that requests and responds to http request
   * 
   * @param retrieveDataFromReply : Will transfer the HttpResponse to the this function to be parsed
   * @param path this String will be used to acquire the full path of the URL to connect to. The path refers to the URL excluding the hostname. Example: "csrf/getTokenJson" not like this: "http://domain.com/csrf/getTokenJson"
   * @param httpType **OPTIONAL** The types are from HttpMethods Example: HttpMethods.GET
   * @param content **OPTIONAL** the body of the http packet to be sent to the server
   * @param header **OPTIONAL** the stuff you need to add to the Header, can be created using RawHeader. Example: Cookie
   */
  def returnResultFromHTTPRequest[T](
      retrieveDataFromReply:(HttpResponse) => Future[T],
      path: String,
      httpType: HttpMethod = HttpMethods.GET,
      content: RequestEntity = HttpEntity.Empty,
      header: HttpHeader = null): Future[T] = {
    
    var request = HttpRequest(
            method = httpType,
            uri = "http://" + connection.hostname + "/" + path,
            entity = content
    )
    
    if(header != null) {
      request = request.addHeader(header)
    }
    
    var responseFuture = Http().singleRequest(request)
    
    responseFuture.andThen {
      case Success(respond) => 
      case Failure(respond) => {
        throw new akka.stream.StreamTcpException("No Internet Connection")
      }
    }
    
    responseFuture.flatMap[T] {
      case someResponse:HttpResponse => {
        retrieveDataFromReply(someResponse)
      }
    }
  }
}