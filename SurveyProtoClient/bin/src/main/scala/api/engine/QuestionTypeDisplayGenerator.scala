package api.engine

import scalafx.scene.layout. { VBox, HBox }
import scalafx.scene.control.{Label, TextArea, TextField, ToggleGroup, RadioButton, CheckBox, Spinner, SpinnerValueFactory}
import scalafx.scene.text.Font
import scalafx.scene.input.{ MouseEvent, KeyEvent }
import scalafx.Includes._
import scalafx.geometry.Insets
import scalafx.beans.property.{ BooleanProperty, ReadOnlyBooleanProperty }

import scala.collection.mutable.{ ArrayBuffer, ListMap }

import java.lang.reflect.Field

import api.models.questionaireanswermodel._
import api.models._

/** Control engine for the survey ExcessiveInternetSurvey
 * @todo Make the answer generator be more generic
 * @note Currently only supports ExcessiveInternetSurvey answers
 * @note Singleton
 * @example {{{
 * QuestionTypeDisplayGenerator.getInstance(fontFamily, paneWidth)
 * }}}
 */
class QuestionTypeDisplayGenerator private (var fontFamily: String, var panewidth: Int) {
  private var event: (_>:scalafx.scene.input.InputEvent) => Unit = null
  private var arrOfId: ArrayBuffer[String] = null
  private var location: ListMap[String, Option[Boolean]] = null
  private var doingOnNet: ListMap[String, Option[Boolean]] = null
  private var onlineDevice: ListMap[String, Option[Boolean]] = null
  private var characterize: ListMap[String, Int] = null
  private var apply: ListMap[String, Int] = null
  private var describe: ListMap[String, Int] = null
  private var questionAnswer: ListMap[String, String] = null
  
  /** Method used to generate the controls for the survey
   * @param survey the survey object
   * @param keyOrMouseEvent this function  will be added to controls like the radio button, checkbox and text field/area.
   * @return A tupple ._1 is the VBox containing the controls and ._2 containing the ID of controls
   */
  def generateRootLayoutSurvey(survey: Survey, keyOrMouseEvent:(_>:scalafx.scene.input.InputEvent) => Unit): (VBox, ArrayBuffer[String]) = {
    event = keyOrMouseEvent
    arrOfId = ArrayBuffer[String]()
    val rootVBox = new VBox(spacing = 20) {
      id = survey.name
      layoutX = 10
      styleClass = Array[String]("rootVBox")
    }
//    val toggleGroupArr = ArrayBuffer[ToggleGroup]()
    val questions = survey.questions
    val mainLabel = new Label(new javafx.scene.control.Label) {
      text = survey.name
      font = Font(
          family = fontFamily,
          weight = javafx.scene.text.FontWeight.BOLD,
          size = 20
      )
    }
    rootVBox.children+=mainLabel
    questions.foreach(
      question => {
        val subVBox = new VBox(spacing = 10)
        if (question.isInstanceOf[ScalingQuestion]) {
          val convertedQuestion = question.asInstanceOf[ScalingQuestion]
          val index = convertedQuestion.index
          val name = convertedQuestion.name
          val questionTitle = convertedQuestion.text
          val questionStyles = convertedQuestion.questionStyles
          val questionScaling = convertedQuestion.scaling
          val questionTitleLabel = new Label(new javafx.scene.control.Label) {
            text = "Question " + index + ": " + questionTitle
            font = Font(
                family = fontFamily,
                weight = javafx.scene.text.FontWeight.BOLD,
                size = 18
            )
            prefWidth = panewidth - 21 
            wrapText = true
          }
          subVBox.children+=questionTitleLabel
          questionStyles.zipWithIndex.foreach {
            case (qStyle, index) => {
              val scaling = new VBox(spacing = 6) {
                id = question.index + "scalingIndex" + index
              }
              val toggleGroup = new ToggleGroup(new javafx.scene.control.ToggleGroup)
              questionScaling.foreach(
                scale => {
                  val radio = new RadioButton(scale.desc) {
                    font = Font(
                      family = fontFamily,
                      size = 14
                    )
                    id = question.name + "-" + qStyle.name + "-" + scale.value
                    onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
                  }
                  arrOfId+=radio.id.value
                  scaling.children+=radio
                  toggleGroup.toggles+=radio
                }
              )
              val convertedStyle = qStyle.asInstanceOf[ScalingStatement]
              val scaleQuestLabel = new Label(new javafx.scene.control.Label) {
                text = convertedStyle.question
                font = Font(
                    family = fontFamily,
                    size = 14
                )
                prefWidth = panewidth - 21 
                wrapText = true
              }
              subVBox.children+=scaleQuestLabel
              subVBox.children+=scaling
//              toggleGroupArr.append(toggleGroup)
            }
          }
          subVBox.styleClass = Array[String]("questionVBox")
          subVBox.padding = Insets(10)
          subVBox.margin = Insets(0,10,0,0)
          rootVBox.children+=subVBox
        } else if (question.isInstanceOf[Question]){
          val index = question.index
          val name = question.name
          val questionTitle = question.text
          val questionStyles = question.questionStyles
          
          questionStyles.foreach(
            styles => {
              val tuppleOfQuestions = questionStylesHandler(styles, index, false, questionTitle)
              subVBox.children+=tuppleOfQuestions
//              if (tuppleOfQuestions._2 != None) {
//                tuppleOfQuestions._2.get.foreach(
//                  tGroup =>
//                    toggleGroupArr.append(tGroup)
//                )
//              }
              subVBox.styleClass = Array[String]("questionVBox")
              subVBox.padding = Insets(10)
              subVBox.margin = Insets(0,10,0,0)
              rootVBox.children+=subVBox
            }
          )
        }
      }
    )
    (rootVBox, arrOfId)
  }
  
  /** Needed for the generateRootLayoutSurveyWithAnswer to work
   * 
   * @todo Make this method better at handling any answers given by the implementer
   */
  def splitAnswer (answer: ExcessiveInternetSurvey) {
    location = ListMap[String, Option[Boolean]](
      "homeUsage" -> None,
	    "hotspotUsage"-> None
    )
    doingOnNet = ListMap[String, Option[Boolean]](
      "searchInfo" -> None,
    	"readNews" -> None,
    	"writeForum" -> None,
    	"useSocial" -> None,
    	"playgame" -> None    
    )
    onlineDevice = ListMap[String, Option[Boolean]](
      "pc" -> None,
    	"laptop" -> None,
    	"tablet" -> None,
    	"mobile" -> None
    )
    characterize = ListMap[String, Int](
        "characterize1" -> 0,
        "characterize2" -> 0,
        "characterize3" -> 0,
        "characterize4" -> 0,
        "characterize5" -> 0,
        "characterize6" -> 0,
        "characterize7" -> 0,
        "characterize8" -> 0,
        "characterize9" -> 0,
        "characterize10" -> 0,
        "characterize11" -> 0,
        "characterize12" -> 0,
        "characterize13" -> 0,
        "characterize14" -> 0,
        "characterize15" -> 0,
        "characterize16" -> 0,
        "characterize17" -> 0,
        "characterize18" -> 0
    )
    apply = ListMap[String, Int](
        "apply1" -> 0,
        "apply2" -> 0,
        "apply3" -> 0,
        "apply4" -> 0,
        "apply5" -> 0,
        "apply6" -> 0,
        "apply7" -> 0,
        "apply8" -> 0,
        "apply9" -> 0,
        "apply10" -> 0,
        "apply11" -> 0,
        "apply12" -> 0,
        "apply13" -> 0,
        "apply14" -> 0,
        "apply15" -> 0,
        "apply16" -> 0,
        "apply17" -> 0,
        "apply18" -> 0,
        "apply19" -> 0,
        "apply20" -> 0,
        "apply21" -> 0
    )
    describe = ListMap[String, Int](
        "describey1" -> 0,
        "describey2" -> 0,
        "describey3" -> 0,
        "describey4" -> 0,
        "describey5" -> 0,
        "describey6" -> 0,
        "describey7" -> 0,
        "describey8" -> 0,
        "describey9" -> 0,
        "describey10" -> 0,
        "describey11" -> 0,
        "describey12" -> 0,
        "describey13" -> 0,
        "describey14" -> 0,
        "describey15" -> 0,
        "describey16" -> 0,
        "describey17" -> 0,
        "describey18" -> 0,
        "describey19" -> 0,
        "describey20" -> 0
    )
    questionAnswer = ListMap[String, String](
        "gender" -> "",
        "age" -> "",
      	"internetAge" -> "",
      	"averageHour" -> "",
      	"homeHours"-> "",
      	"outsideHours" -> "",
      	"otherPlaces" -> "",
      	"otherPlacesHours" -> "",
      	"otherDoing" -> "",
      	"otherDevices" -> ""
    )
    
    println(answer.homeHours, answer.outsideHours, answer.otherPlacesHours)
    answer.getClass.getDeclaredFields.foreach {
      case f: Field => {
        f.setAccessible(true)
        val name = f.getName
        name match {
          case "locations" => {
            println("location")
            val mfLocation = f.get(answer).asInstanceOf[LocationUsage]
            mfLocation.getClass.getDeclaredFields.foreach {
              case mf: Field => {
                mf.setAccessible(true)
                location.update(mf.getName, mf.get(mfLocation).asInstanceOf[Option[Boolean]])
              }
            }
          }
          case "doingOntNet" => {
            println("doingOntNet")
            val mfDoingOnNet = f.get(answer).asInstanceOf[DoingOnNet]
            mfDoingOnNet.getClass.getDeclaredFields.foreach {
              case mf: Field => {
                mf.setAccessible(true)
                doingOnNet.update(mf.getName, mf.get(mfDoingOnNet).asInstanceOf[Option[Boolean]])
              }
            }
          }
          case "onlineDevices" => {
            println("onlineDevices")
            val mfOnlineDevices = f.get(answer).asInstanceOf[OnlineDevice]
            mfOnlineDevices.getClass.getDeclaredFields.foreach {
              case mf: Field => {
                mf.setAccessible(true)
                onlineDevice.update(mf.getName, mf.get(mfOnlineDevices).asInstanceOf[Option[Boolean]])
              }
            }
          }
          case "applyQuestion" => {
            println("applyQuestion")
            val mfApplyQuestion = f.get(answer).asInstanceOf[ApplyQuestion]
            mfApplyQuestion.getClass.getDeclaredFields.foreach {
              case mf: Field => {
                mf.setAccessible(true)
                apply.update(mf.getName, mf.get(mfApplyQuestion).asInstanceOf[Int])
              }
            }
          }
          case "characterizeQuestion" => {
            println("characterizeQuestion")
            val mfCharacterizeQuestion = f.get(answer).asInstanceOf[CharacterizeQuestion]
            mfCharacterizeQuestion.getClass.getDeclaredFields.foreach {
              case mf: Field => {
                mf.setAccessible(true)
                characterize.update(mf.getName, mf.get(mfCharacterizeQuestion).asInstanceOf[Int])
              }
            }
          }
          case "describeYouQuestion" => {
            println("describeYouQuestion")
            val mfDescribeQuestion = f.get(answer).asInstanceOf[DescribeYouQuestion]
            mfDescribeQuestion.getClass.getDeclaredFields.foreach {
              case mf: Field => {
                mf.setAccessible(true)
                describe.update(mf.getName, mf.get(mfDescribeQuestion).asInstanceOf[Int])
              }
            }
          }
          case _ => {
            try {
              questionAnswer.update(name, f.get(answer).asInstanceOf[String])
              println(f.get(answer).asInstanceOf[String], "second")
            } catch {
               case e: ClassCastException => {
                 try {
                   questionAnswer.update(name, String.valueOf(f.get(answer).asInstanceOf[Int]))
                   println(String.valueOf(f.get(answer).asInstanceOf[Int]), "forth")
                 } catch {
                   case e: ClassCastException => {
                     val something = f.get(answer).asInstanceOf[Option[Any]]
                     if (something.get.isInstanceOf[String]) {
                       questionAnswer.update(name, something.get.asInstanceOf[String])
                     } else if (something.get.isInstanceOf[Int]) {
                       questionAnswer.update(name, something.get.asInstanceOf[Int].toString)
                     } else {
                       println("WTH")
                     }
                   }
                 }
               }
            }
          }
        }
      }
    }
  }
  
  /** Method used to generate the controls for the survey
   *
   * @todo Need to make this method to be able to support any answers built from the ExcessiveInternetSurvey
   * @param survey the survey object
   * @param keyOrMouseEvent this function  will be added to controls like the radio button, checkbox and text field/area.
   * @param answer the answers for the survey **ONLY SUPPORTS EXCESSIVEINTERNETSURVEY**
   * @return A tupple ._1 is the VBox containing the controls (disabled and modified according to answers) and ._2 containing the ID of controls
   */
  def generateRootLayoutSurveyWithAnswer(
      survey: Survey,
      keyOrMouseEvent:(_>:scalafx.scene.input.InputEvent) => Unit,
      answer: ExcessiveInternetSurvey): (VBox, ArrayBuffer[String]) = {
    event = keyOrMouseEvent
    arrOfId = ArrayBuffer[String]()
    splitAnswer(answer)
    val rootVBox = new VBox(spacing = 20) {
      id = survey.name
      layoutX = 10
      styleClass = Array[String]("rootVBox")
      disable = true
    }
//    val toggleGroupArr = ArrayBuffer[ToggleGroup]()
    val questions = survey.questions
    val mainLabel = new Label(new javafx.scene.control.Label) {
      text = survey.name
      font = Font(
          family = fontFamily,
          weight = javafx.scene.text.FontWeight.BOLD,
          size = 20
      )
    }
    rootVBox.children+=mainLabel
    questions.foreach(
      question => {
        val subVBox = new VBox(spacing = 10)
        if (question.isInstanceOf[ScalingQuestion]) {
          val convertedQuestion = question.asInstanceOf[ScalingQuestion]
          val index = convertedQuestion.index
          val name = convertedQuestion.name
          val questionTitle = convertedQuestion.text
          val questionStyles = convertedQuestion.questionStyles
          val questionScaling = convertedQuestion.scaling
          val questionTitleLabel = new Label(new javafx.scene.control.Label) {
            text = "Question " + index + ": " + questionTitle
            font = Font(
                family = fontFamily,
                weight = javafx.scene.text.FontWeight.BOLD,
                size = 18
            )
            prefWidth = panewidth - 21 
            wrapText = true
          }
          subVBox.children+=questionTitleLabel
          var listmap: ListMap[String, Int] = null
          questionStyles.zipWithIndex.foreach {
            case (qStyle, index) => {
              if (listmap == null && apply != null) {
                if (qStyle.name.contains("apply")) {
                  listmap = apply
                } else if (qStyle.name.contains("describe")) {
                  listmap = describe
                } else if (qStyle.name.contains("characterize")){
                  listmap = characterize
                }
              }
              println(listmap)
              val scaling = new VBox(spacing = 6) {
                id = question.index + "scalingIndex" + index
              }
              val toggleGroup = new ToggleGroup(new javafx.scene.control.ToggleGroup)
              questionScaling.foreach(
                scale => {
                  var boolToggle = false
                  if (listmap != null) {
                    if (listmap.get(qStyle.name).get == scale.value.toInt) {
                      boolToggle = true
                    }
                  }
                  val radio = new RadioButton(scale.desc) {
                    font = Font(
                      family = fontFamily,
                      size = 14
                    )
                    id = question.name + "-" + qStyle.name + "-" + scale.value
                    onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
                    selected = boolToggle
                  }
                  arrOfId+=radio.id.value
                  scaling.children+=radio
                  toggleGroup.toggles+=radio
                }
              )
              val convertedStyle = qStyle.asInstanceOf[ScalingStatement]
              val scaleQuestLabel = new Label(new javafx.scene.control.Label) {
                text = convertedStyle.question
                font = Font(
                    family = fontFamily,
                    size = 14
                )
                prefWidth = panewidth - 21 
                wrapText = true
              }
              subVBox.children+=scaleQuestLabel
              subVBox.children+=scaling
//              toggleGroupArr.append(toggleGroup)
            }
          }
          subVBox.styleClass = Array[String]("questionVBox")
          subVBox.padding = Insets(10)
          subVBox.margin = Insets(0,10,0,0)
          rootVBox.children+=subVBox
        } else if (question.isInstanceOf[Question]){
          val index = question.index
          val name = question.name
          val questionTitle = question.text
          val questionStyles = question.questionStyles
          
          questionStyles.foreach(
            styles => {
              val tuppleOfQuestions = questionStylesHandler(styles, index, false, questionTitle)
              subVBox.children+=tuppleOfQuestions
//              if (tuppleOfQuestions._2 != None) {
//                tuppleOfQuestions._2.get.foreach(
//                  tGroup =>
//                    toggleGroupArr.append(tGroup)
//                )
//              }
              subVBox.styleClass = Array[String]("questionVBox")
              subVBox.padding = Insets(10)
              subVBox.margin = Insets(0,10,0,0)
              rootVBox.children+=subVBox
            }
          )
        }
      }
    )
    location = null
    doingOnNet = null
    onlineDevice = null
    characterize = null
    apply = null
    describe = null
    questionAnswer = null
    (rootVBox, arrOfId)
  }
  
  /** A method to handle QuestionStyles
   *
   * @param question the QuestionStyle object
   * @param index index of the question in the survey (Question no)
   * @param complexChoice Is this QuestionStyle a ComplexChoice?
   * @param questionTitle the question associated with this QuestionStyle
   * @return A VBox containing the controls based on the QuestionStyle
   */
  def questionStylesHandler(
      question: QuestionStyle, index: Int, complexChoice: Boolean, questionTitle: String = ""): VBox = {
//    val subVBox = new VBox(spacing = 10)
    var tempSubVBox: VBox = null
    var questionTitleLabel: Label = null
    if (!questionTitle.isEmpty) {
      questionTitleLabel = new Label(new javafx.scene.control.Label) {
        text = "Question " + index + ": " + questionTitle
        font = Font(
            family = fontFamily,
            weight = javafx.scene.text.FontWeight.BOLD,
            size = 18
        )
        prefWidth = panewidth - 21 
        wrapText = true
      }
    }
    
//    val toggles = new ArrayBuffer[ToggleGroup]()
    question match {
      case _:SingleMultipleChoice => {
        val convertedStyles = question.asInstanceOf[SingleMultipleChoice]
        val answers = convertedStyles.choices
        val tuppleVBoxToggles = 
          if(complexChoice) displayRadioButtonAnswers(question, index, answers, extra = "complex") 
          else displayRadioButtonAnswers(question, index, answers)
        tempSubVBox = tuppleVBoxToggles._1
//        tuppleVBoxToggles._2.foreach(
//          togglegroups =>
//            toggles.append(togglegroups)
//        )
      }
      case _:MultipleMultipleChoice => {
        val convertedStyles = question.asInstanceOf[MultipleMultipleChoice]
        val answers = convertedStyles.choices
        val tuppleVBoxToggles = 
          if(complexChoice) displayCheckBoxAnswers(question, index, answers, extra = "complex") 
          else displayCheckBoxAnswers(question, index, answers)
        tempSubVBox = tuppleVBoxToggles._1
//        if (tuppleVBoxToggles._2 != None) {
//          tuppleVBoxToggles._2.get.foreach(
//            togglegroups =>
//              toggles.append(togglegroups)
//          )
//        }
      }
      case _:ShortTextQuestion => {
        if(complexChoice) tempSubVBox = displayShortAnswers(question, index, extra = "complex") 
        else tempSubVBox = displayShortAnswers(question, index)
      }
      case _:LongTextQuestion => {
        if(complexChoice) tempSubVBox = displayLongAnswers(question, index, extra = "complex") 
        else tempSubVBox = displayLongAnswers(question, index)
      }
      case _:NumberQuestion => {
        val convertedQuestion = question.asInstanceOf[NumberQuestion]
        if (convertedQuestion.limit == None) {
          if(complexChoice) tempSubVBox = displayNumberAnswer(question, index, 0, Integer.MAX_VALUE, 0, extra = "complex") 
          else tempSubVBox = displayNumberAnswer(question, index, 0, Integer.MAX_VALUE, 0)
        } else {
          val limit = convertedQuestion.limit.get
          if(complexChoice) tempSubVBox = displayNumberAnswer(question, index, limit._1, limit._2, limit._1, extra = "complex") 
          else tempSubVBox = displayNumberAnswer(question, index, limit._1, limit._2, limit._1)
        }
      }
      case _:DecimalQuestion => {
        if(complexChoice) tempSubVBox = displayDecimalAnswer(question, index, 0.0, Double.MaxValue, 0.0, extra = "complex") 
        else tempSubVBox = displayDecimalAnswer(question, index, 0.0, Double.MaxValue, 0.0)
      }
      case _:BooleanQuestion => {
        var tuppleVBoxToggles: (VBox, ArrayBuffer[ToggleGroup]) = null 
        if(complexChoice) {
          tuppleVBoxToggles = displayRadioButtonAnswers(
            question,
            index,
            Array(
                SimpleChoice(name="True", text="True"),
                SimpleChoice(name="False", text="False")
            ),
            extra = "complex"
          )
        } else {
         tuppleVBoxToggles = displayRadioButtonAnswers(
            question,
            index,
            Array(
                SimpleChoice(name="True", text="True"),
                SimpleChoice(name="False", text="False")
            )
          )
        }
        tempSubVBox = tuppleVBoxToggles._1
//        tuppleVBoxToggles._2.foreach(
//          togglegroups =>
//            toggles.append(togglegroups)
//        )
      }
      case _:Any => {
        println("If you reach here you have done something wrong lol")
      }
    }
    val subVBox = new VBox(spacing = tempSubVBox.spacing.toInt)
    subVBox.id = tempSubVBox.id.value
    if (questionTitleLabel != null) subVBox.children+=questionTitleLabel
    subVBox.children+=tempSubVBox
    subVBox
  }
  
  def displayCheckBoxAnswers(question: QuestionStyle, index: Int, answers: Array[Choice], extra: String = ""): (VBox, Option[ArrayBuffer[ToggleGroup]]) = {
    val subVBox = new VBox(spacing = 10) {
      id = "non-scalingCheckBox" + extra + index
    }
    val toggles = ArrayBuffer[ToggleGroup]()
    answers.foreach(
      answer => {
        var boolSel = false
        if (doingOnNet != null) {
          if (question.name.contains("doing") && doingOnNet.get(answer.name) != None) {
            boolSel = doingOnNet.get(answer.name).get.get
            println(1)
          } else if (question.name.contains("location") && location.get(answer.name) != None) {
            boolSel = location.get(answer.name).get.get
            println(2)
          } else if (question.name.contains("online") && onlineDevice.get(answer.name) != None) {
            boolSel = onlineDevice.get(answer.name).get.get
            println(3)
          }
        }
        if (answer.isInstanceOf[SimpleChoice]) {
          println(boolSel)
          val radio = new CheckBox(answer.text) {
            font = Font(
              family = fontFamily,
              size = 14
            )
            id = question.name + "-" + answer.name + "-" + extra + index
            onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
            selected = boolSel
          }
          arrOfId+=radio.id.value
          subVBox.children+=radio
        } else {
          val subHBox = new HBox(spacing = 5)
          val convertedAns = answer.asInstanceOf[ComplexChoice]
          val radio = new CheckBox(answer.text) {
            font = Font(
              family = fontFamily,
              size = 14
            )
            id = answer.name + "-" + extra + index
            onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
            selected = boolSel
          }
          arrOfId+=radio.id.value
          subHBox.children+=radio
          val complexVBox = new VBox(spacing = 10) {
            disable = true
          }
          convertedAns.style.foreach(
              complexStyle => {
                val returnResult = questionStylesHandler(complexStyle, index, true)
                complexVBox.children+=returnResult
//                if (returnResult._2 != None) {
//                  returnResult._2.get.foreach(
//                    toggleArray =>
//                      toggles.append(toggleArray)
//                  )
//                }
              }
          )
          subHBox.children+=complexVBox
          subVBox.children+=subHBox
        }
      }
    )
    if (toggles.isEmpty) {
      (subVBox, None)
    } else {
      (subVBox, Some(toggles))
    }
  }
  
  def displayRadioButtonAnswers(question: QuestionStyle, index: Int, answers: Array[Choice], extra: String = ""): (VBox, ArrayBuffer[ToggleGroup]) = {
    val subVBox = new VBox(spacing = 10)
    val toggle = new ToggleGroup(new javafx.scene.control.ToggleGroup)
    val toggles = ArrayBuffer[ToggleGroup]()
    var answerName = ""
    if (questionAnswer != null) {
      if (questionAnswer.get(question.name).get != None) {
        answerName = questionAnswer.get(question.name).get.asInstanceOf[String]
      }
    }
    answers.foreach(
      answer => {
        if (answer.isInstanceOf[SimpleChoice]) {
          var boolToggle = false
          if (answerName.equals(answer.name)) {
            boolToggle = true
          }
          val radio = new RadioButton(answer.text) {
            font = Font(
              family = fontFamily,
              size = 14
            )
            onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
            id = question.name + "-" + answer.name + "-" + extra + index
            selected = boolToggle
          }
          arrOfId+=radio.id.value
          toggle.toggles+=radio
          subVBox.children+=radio
        } else {
          val subHBox = new HBox(spacing = 5)
          val convertedAns = answer.asInstanceOf[ComplexChoice]
          val radio = new RadioButton(answer.text) {
            font = Font(
              family = fontFamily,
              size = 14
            )
            onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
            id = question.name + "-" + answer.name + "-" + extra + index
          }
          arrOfId+=radio.id.value
          toggle.toggles+=radio
          subHBox.children+=radio
          val complexVBox = new VBox(spacing = 10) {
            disable = true
          }
          convertedAns.style.foreach(
              complexStyle => {
                val returnResult = questionStylesHandler(complexStyle, index, true)
                complexVBox.children+=returnResult
//                if (returnResult._2 != None) {
//                  returnResult._2.get.foreach(
//                    toggleArray =>
//                      toggles.append(toggleArray)
//                  )
//                }
              }
          )
          subHBox.children+=complexVBox
          subVBox.children+=subHBox
        }
      }
    )
    toggles.append(toggle)
    (subVBox, toggles)
  }
  
  def displayLongAnswers(question: QuestionStyle, index: Int, extra: String = ""): VBox = {
    val layout = new VBox(spacing = 10) {
//      id = "non-scalingLong" + extra + index
//      onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
    }
    val textInput = new TextArea(new javafx.scene.control.TextArea) {
      font = Font(
          family = fontFamily,
          size = 14
      )
      prefRowCount = 3
      prefWidth = panewidth - 21
      wrapText = true
      id = question.name + "-" + extra + index
      onKeyTyped = (eventParam: KeyEvent) => event(eventParam)
    }
    arrOfId+=textInput.id.value
    
    layout.children+=textInput
    layout
  }
  
  def displayShortAnswers(question: QuestionStyle, index: Int, extra: String = ""): VBox = {
    val layout = new VBox(spacing = 10) {
//      id = "non-scalingShort" + extra + index
    }
    var textVal = ""
    if (questionAnswer != null) {
      if (questionAnswer.get(question.name).get != None) {
        textVal = questionAnswer.get(question.name).get.asInstanceOf[String]
      }
    }
    val textInput = new TextField(new javafx.scene.control.TextField) {
      font = Font(
          family = fontFamily,
          size = 14
      )
      onKeyTyped = (eventParam: KeyEvent) => event(eventParam)
      maxWidth = 300
      id = question.name + "-" + extra + index
      text = textVal
    }
    arrOfId+=textInput.id.value
    
    layout.children+=textInput
    layout
  }
  
  def displayNumberAnswer(question: QuestionStyle, index: Int, min: Int, max: Int, initial: Int, extra: String = ""): VBox = {
    val layout = new VBox(spacing = 10) {
//      id = "non-scalingNumeric" + extra + index
//      onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
//      onKeyTyped = (eventParam: KeyEvent) => event(eventParam)
    }
    var intVal = 0
    if (questionAnswer != null) {
      if (questionAnswer.get(question.name).get != None) {
        println(questionAnswer.mkString("\n"))
        println(question.name, ":DDDDDDDDDDDDDDDDDDDDDD")
        intVal = Integer.valueOf(questionAnswer.get(question.name).get)
      }
    }
    val numberInput = new Spinner[Int](min, max, initial) {
      id = question.name + "-" + extra + index
      onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
    }
    println(intVal, ":DDDDDDDDDDDDDDDDDDDDD")
    numberInput.valueFactory.value.setValue(intVal.toInt)
    arrOfId+=numberInput.id.value
    
    layout.children+=numberInput
    layout
  }
  
  def displayDecimalAnswer(question: QuestionStyle, index: Int, min: Double, max: Double, initial: Double, extra: String = ""): VBox = {
    val layout = new VBox(spacing = 10) {
      id = "non-scalingDouble" + extra + index
      onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
      onKeyTyped = (eventParam: KeyEvent) => event(eventParam)
    }
    val numberInput = new Spinner[Double](min, max, initial) {
      id = question.name + "-" + extra + index
      onMouseClicked = (eventParam: MouseEvent) => event(eventParam)
    }
    arrOfId+=numberInput.id.value
    
    layout.children+=numberInput
    layout
  }
}

object QuestionTypeDisplayGenerator {
  
  private var questionTypeDisplayGenerator: QuestionTypeDisplayGenerator = null
  
  def apply(fontFamily: String = "Arial", width: Int):QuestionTypeDisplayGenerator = {
    if (questionTypeDisplayGenerator == null) {
      questionTypeDisplayGenerator = new QuestionTypeDisplayGenerator(fontFamily, width)
    } 
    questionTypeDisplayGenerator
  }

}