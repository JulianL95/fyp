package api.models.authdetailsmodel

/*
 * @author Julian Leong Jhou Yeaw
 */
import akka.actor.{Actor, ActorRef, Cancellable, Props}

import scala.concurrent.duration._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.ArrayBuffer

import java.net.InetAddress
import java.util.concurrent.atomic.AtomicBoolean

import org.joda.time.DateTime

import api.linkage.Connection
import api.utils.ImplicitUtilities._

/** Case class for Client **IMMUTABLE**
 * username is an email
 * 
 * @param _username username for login
 * @param _password password for login
 */
case class Client (private val _username: String, private val _password: String) {
  private val usernameArr = _username.split("@")
  private val usernameURL = usernameArr(0) + "%40" + usernameArr(1)
  /** Get username after URL conversion
   * 
   * @return username with '%40'
   */
  def username = _username
  /** Get username before URL conversion
   * 
   * @return username with '@'
   */
  def usernameTouched = usernameURL
  /** Get password
   * 
   * @return returns password
   */
  def password = _password
}

/** A case class for session token
 * 
 * @note expiresOn variable if given None it is assume never expires. If scheduled to be refreshed NoneDateTimeException exception will be thrown
 * @param token : a string token for sessions
 * @param expiresOn : the expiry date of the token
 */
case class SessionToken(token: String, expiresOn: Option[DateTime])
/** A case class for submission token
 * 
 * @param token : a string token for submissions
 * @param expiresOn : the expiry date of the token
 * @note expiresOn variable if given None it is assume never expires. If scheduled to be refreshed NoneDateTimeException exception will be thrown
 */
case class SubmissionAuthToken(token: String, expiresOn: Option[DateTime])
/** A class to retrieve token from server
 * 
 * @tparam T : the type of token
 * @tparam U : the type of data to be given to the getToken function
 */
abstract class TokenRetriever[T, U] extends Serializable {
  var token: T
  /** A method to get the Initial Token. Needed to be called first before calling other methods
   * 
   * @param getToken : a function that returns a Future that contains the token
   */
  def getInitialToken (getToken:(U) => Future[T])
  /** A method to stop the scheduler of an actor
   * 
   */
  def stopScheduler
}

/** A trait that supports fallback if problems occured when running getToken function (not connection problems)
 * 
 */
trait RefreshTokenFallbackable[T, U]{
  /** A method to schedule a refresh for the token (stable but slow connection)
   * 
   * @param intervals : the time needed to wait before the next refresh
   * @param getToken : function to execute when the scheduled time has been reached
   * @param fallbackGetTokenIfTokenExpired: function to execute if getToken fails
   */
  def scheduleRefreshToken (intervals: Int, getToken:(U) => Future[T], fallbackGetTokenIfTokenExpired:(U) => Future[T])
  /** A method to schedule a refresh for the token (unstable internet connection, random disconnections)
   * 
   * @param getToken : function to execute when the scheduled time has been reached
   * @param fallbackGetTokenIfTokenExpired : function to execute if getToken fails
   */
  def refreshTokenWheneverPossible(getToken:(U) => Future[T], fallbackGetTokenIfTokenExpired:(U) => Future[T])
}

/** A trait that do not support fallbacks meaning it doesn't fails to get the token
 * 
 */
trait RefreshTokenNotFallbackable[T, U]{
  /** Schedule a refresh for the token (stable but slow connection)
   * 
   * @param intervals : the time needed to wait before the next refresh
   * @param getToken : function to execute when the scheduled time has been reached
   */
  def scheduleRefreshToken (intervals: Int, getToken:(U) => Future[T])
  /** A method to schedule a refresh for the token (unstable internet connection, random disconnections)
   * 
   * @param getToken : function to execute when the scheduled time has been reached
   */
  def refreshTokenWheneverPossible(getToken:(U) => Future[T])
}

/** Submission Token retriever
 * @param connection : connection used for retrieving the submission details
 * @param sessionDetails : the session details needed for acquiring the submission details example: session token.
 */
class SubmissionDetailsRetriever(connection: Connection, val sessionDetails: ClientSessionDetails) extends TokenRetriever[SubmissionAuthToken, SessionToken] with RefreshTokenNotFallbackable[SubmissionAuthToken, SessionToken]{
  var token: SubmissionAuthToken = SubmissionAuthToken("", Option(DateTime.now))
  private var refresherCanceller: Cancellable = null
  private var refresherActor: ActorRef = null
  private val checkingTokenFlag = new AtomicBoolean(false)
  
  /*
   * This will get the initial token from the html file
   * One thing this method do not have is the expiry date of the token
   */
  override def getInitialToken(getToken:(SessionToken) => Future[SubmissionAuthToken]) {
    connection.registerTaskOnConnected(
        (something: InetAddress) => {
          getToken(sessionDetails.token).foreach(
              tokenProps => token.synchronized {
                println("before", token)
                token = tokenProps
                println("after", token)
              }
          )
        }
      )
  }

  /**Will Run the getToken function every intervals counted in seconds
   * If at the moment it started refreshing the token there is no internet connection or the
   * internet connection has been cut off the process will still continue but it will fail
   * 
   * Preferably used when the connection is slow rather than unstable
   * 
   * token will be updated if it is successful
   * 
   * Calling this method when there are no expiry date on token will throw a NoneDateTimeException
   *
   *
   * @throws GetTokenConflictException : If refreshTokenWheneverPossible is called before this method
   * @throws NoneDateTimeException : If token expiresOn variable is None and this method is called
   */
  override def scheduleRefreshToken(
      intervals: Int = 0,
      getToken:(SessionToken) => Future[SubmissionAuthToken]
      ) {
    checkingTokenFlag.synchronized {
      if (!checkingTokenFlag.get) {
        checkingTokenFlag.set(!checkingTokenFlag.get)
      } else {
        throw new GetTokenConflictException
      }
    }
    refresherActor = system.actorOf(Props(classOf[SubmissionDetailsRetrieverRefresher], this, intervals, 
        (client: Client) => {
          if (token.expiresOn == None) {
            throw new NoneDateTimeException
          }
          getToken(sessionDetails.token)
        }
    ), "SubmissionDetailsRetrieverRefresherActor")
    refresherActor ! "SCHEDULE"
  }

  /**Will run the getToken function whenever there is an internet connection
   * Uses the Connection companion object function registerTaskOnConnected
   * 
   * This is used for unstable internet connection
   * 
   * Only one of the two token refreshing method can be used to refresh the token if both are called a RuntimeException
   * will be thrown
   * 
   * Calling this method when there are no expiry date on token will throw a NoneDateTimeException
   * from superclass DataRetrievalDetails
   *
   * @throws GetTokenConflictException : If scheduleRefreshToken is called before this method
   * @throws NoneDateTimeException : If token expiresOn variable is None and this method is called
   */
  override def refreshTokenWheneverPossible(getToken:(SessionToken) => Future[SubmissionAuthToken]) {
    checkingTokenFlag.synchronized {
      if (!checkingTokenFlag.get) {
        checkingTokenFlag.set(!checkingTokenFlag.get)
      } else {
        throw new GetTokenConflictException
      }
    }
    connection.registerTaskOnConnected(
      (something: InetAddress) => {
        if (!token.token.isEmpty) {
          var fut: Future[SubmissionAuthToken] = null
          if (token.expiresOn == None) {
            throw new NoneDateTimeException
          }
          fut = getToken(sessionDetails.token)
          fut.foreach(
            tokenProps => token.synchronized {
              token = tokenProps
            }
          )
        }
      }
    )
  }
  
  override def stopScheduler {
    if (refresherCanceller != null) refresherCanceller.cancel
  }

  private class SubmissionDetailsRetrieverRefresher(var intervals: Int, getToken:(SessionToken) => Future[SubmissionAuthToken]) extends Actor {
    override def receive = {
      case "START" => {
        connection.registerTaskOnConnected(
          (address: InetAddress) => {
            if (!token.token.isEmpty) {
              getToken(sessionDetails.token).foreach(
                tokenProps => token.synchronized {
                  println("before", token)
                  token = tokenProps
                  println("after", token)
                }
              )
            }
          }
        )
      }
      case "SCHEDULE" => {
        if (intervals == 0) intervals = 60
        refresherCanceller = system.scheduler.schedule(Duration.Zero, Duration(intervals, SECONDS), self, "START")
      }
      case _ => 
    }
  }
}

/** Exception for multiple scheduling of refreshing token
 * 
 */
class GetTokenConflictException extends RuntimeException
/** Exception for scheduling a refresh token task when the token is not expirable
 *
 */
class NoneDateTimeException extends RuntimeException

/** Session details retriever
 *
 * @param connection : the connection needed to acquire the session details
 * @param _client : the client details needed to acquire the session details example: username, password.
 */
class ClientSessionDetails(connection: Connection, var _client: Client) extends TokenRetriever[SessionToken, Client] with RefreshTokenFallbackable[SessionToken, Client]{
  private var refresherCanceller: Cancellable = null
  private var refresherActor: ActorRef = null
  private val checkingTokenFlag = new AtomicBoolean(false)
  var token: SessionToken = SessionToken("", Option(DateTime.now))
  
  def client_=(value: Client) = _client.synchronized { _client = value }
  def client: Client = _client.synchronized { _client }
  /*
   * This will get the initial token from the html file
   * One thing this method do not have is the expiry date of the token
   */
  override def getInitialToken(getToken:(Client) => Future[SessionToken]) {
    connection.registerTaskOnConnected(
        (something: InetAddress) => {
          getToken(_client).foreach(
              tokenProps => token.synchronized {
                println("before", token)
                token = tokenProps
                println("after", token)
              }
          )
        }
      )
  }

  /**Will Run the getToken function every intervals counted in seconds
   * If at the moment it started refreshing the token there is no internet connection or the
   * internet connection has been cut off the process will still continue but it will fail
   * 
   * Preferably used when the connection is slow rather than unstable
   * 
   * token will be updated if it is successful
   * 
   * Calling this method when there are no expiry date on token will throw a NoneDateTimeException
   * from superclass DataRetrievalDetails
   *
   * @throws GetTokenConflictException : If refreshTokenWheneverPossible is called before this method
   * @throws NoneDateTimeException : If token expiresOn variable is None and this method is called
   */
  override def scheduleRefreshToken(intervals: Int = 0, getToken:(Client) => Future[SessionToken], fallbackGetTokenIfTokenExpired:(Client) => Future[SessionToken]) {
    checkingTokenFlag.synchronized {
      if (!checkingTokenFlag.get) {
        checkingTokenFlag.set(!checkingTokenFlag.get)
      } else {
        throw new GetTokenConflictException
      }
    }
    refresherActor = system.actorOf(Props(classOf[SessionDetailsRefresher], this, intervals, 
        (client: Client) => {
          if (token.expiresOn == None) {
            throw new NoneDateTimeException
          }
          if (!token.expiresOn.get.isBefore(DateTime.now)) {
            getToken(client)
          } else {
            fallbackGetTokenIfTokenExpired(client)
          }
        }
    ), "SessionDetailsRefresher")
    refresherActor ! "SCHEDULE"
  }

  /**Will run the getToken function whenever there is an internet connection
   * Uses the Connection companion object function registerTaskOnConnected
   * 
   * This is used for unstable internet connection
   * 
   * Only one of the two token refreshing method can be used to refresh the token if both are called a RuntimeException
   * will be thrown
   * 
   * Calling this method when there are no expiry date on token will throw a NoneDateTimeException
   * from superclass DataRetrievalDetails
   *
   * @throws GetTokenConflictException : If scheduleRefreshToken is called before this method
   * @throws NoneDateTimeException : If token expiresOn variable is None and this method is called
   */
  override def refreshTokenWheneverPossible(getToken:(Client) => Future[SessionToken], fallbackGetTokenIfTokenExpired:(Client) => Future[SessionToken]) {
    checkingTokenFlag.synchronized {
      if (!checkingTokenFlag.get) {
        checkingTokenFlag.set(!checkingTokenFlag.get)
      } else {
        throw new GetTokenConflictException
      }
    }
    connection.registerTaskOnConnected(
      (something: InetAddress) => {
        if (!token.token.isEmpty) {
          var fut: Future[SessionToken] = null
          if (token.expiresOn == None) {
            throw new NoneDateTimeException
          }
          if (!token.expiresOn.get.isBefore(DateTime.now)) {
            fut = getToken(_client)
          } else {
            fut = fallbackGetTokenIfTokenExpired(_client)
          }
          fut.foreach(
            tokenProps => token.synchronized {
              token = tokenProps
            }
          )
        }
      }
    )
  }
  
  override def stopScheduler {
    if (refresherCanceller != null) refresherCanceller.cancel
  }

  private class SessionDetailsRefresher(var intervals: Int, getToken:(Client) => Future[SessionToken]) extends Actor {
    override def receive = {
      case "START" => {
        connection.registerTaskOnConnected(
          (address: InetAddress) => {
            if (!token.token.isEmpty) {
              getToken(_client).foreach(
                tokenProps => token.synchronized {
                  println("before", token)
                  token = tokenProps
                  println("after", token)
                }
              )
            }
          }
        )
      }
      case "SCHEDULE" => {
        if (intervals == 0) intervals = 60
        refresherCanceller = system.scheduler.schedule(Duration.Zero, Duration(intervals, SECONDS), self, "START")
      }
      case _ => 
    }
  }
}