package api.models

/**
 * This class represent the root of question's pattern
 */
sealed abstract class QuestionStyle {
  def name: String
  def question: String
}

/**
 * This class represent the root of all Questions
 * @param id the unique identification of a question
 * @param index the number of question in list of question
 * @param name the name of the question
 * @param text the question text
 */
class Question(
  val index: Int, val name: String, val text: String) extends Serializable {
  private var _questionStyles: Array[QuestionStyle] = Array()
  def questionStyles = _questionStyles
  protected def questionStyles_=(arr: Array[QuestionStyle]) {
    _questionStyles = arr
  }
}

case class ScalingValue(desc: String, value: Double)

class ScalingQuestion(_index: Int, _name: String,
  _text: String, _questionStyles: Array[QuestionStyle], val scaling: Array[ScalingValue]) extends Question(_index, _name, _text) {
  questionStyles = _questionStyles
}

/**
 * This class represent the Survey
 */
class Survey (val name: String, val questions: Array[Question]){
  val uuid = java.util.UUID.randomUUID
}

/**
 * This class represent a choice answer in Multiple choice question
 */
abstract class Choice {
  def name: String //due to certain circumstance, checkbox is used
  def text: String
}
 /**
 * This class represent a simple choice answer in Multiple choice question
 */
case class SimpleChoice(name: String, text: String) extends Choice

/**
 * This class represent a composite choice answer in Multiple choice question
 */
case class ComplexChoice(name: String, text: String, style: Array[QuestionStyle]) extends Choice

/**
 * This class represent a single answer in Multiple choice question
 */
case class SingleMultipleChoice(name: String, question: String,
  val choices: Array[Choice])
  extends QuestionStyle
/**
 * This class represent the Multiple selection style
 */
case class MultipleMultipleChoice(name: String, question: String,
  val choices: Array[Choice])
  extends QuestionStyle
/**
 * This class represent the Short Text style
 */
case class ShortTextQuestion(name:String, question: String) extends QuestionStyle

/**
 * This class represent the Long Text style
 */
case class LongTextQuestion(name: String, question: String) extends QuestionStyle

/**
 * This class represent the number style
 */
case class NumberQuestion(name: String, question: String, limit: Option[(Int, Int)] = None) extends QuestionStyle

/**
 * This class represent the decimal style
 */
case class DecimalQuestion(name: String, question: String) extends QuestionStyle

/**
 * This class represent the boolean True / false style
 */
case class BooleanQuestion(name: String, question: String) extends QuestionStyle

/**
 * This class represent the scalling question that request user to give scaling value
 */
case class ScalingStatement(name: String, question: String) extends QuestionStyle

