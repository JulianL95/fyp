//= java.util.UUID.randomUUID
//trait represent Answer
package api.models
import org.joda.time.DateTime

case class SurveyAnswer[T] (
	surveyId: String ,
	email: String ,
	time: DateTime,
	survey: T
)

