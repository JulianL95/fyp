package api.models.questionaireanswermodel

import api.models._
import spray.json._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

case class LocationUsage(
	homeUsage: Option[Boolean],
	hotspotUsage: Option[Boolean]
	)

case class DoingOnNet (
	searchInfo: Option[Boolean],
	readNews: Option[Boolean],
	writeForum: Option[Boolean],
	useSocial: Option[Boolean],
	playgame: Option[Boolean]
	)
	
case class OnlineDevice (
	pc: Option[Boolean],
	laptop: Option[Boolean],
	tablet: Option[Boolean],
	mobile: Option[Boolean]
  )
  
case class CharacterizeQuestion (
	characterize1: Int,
	characterize2: Int,
	characterize3: Int,
	characterize4: Int,
	characterize5: Int,
	characterize6: Int,
	characterize7: Int,
	characterize8: Int,
	characterize9: Int,
	characterize10: Int,
	characterize11: Int,
	characterize12: Int,
	characterize13: Int,
	characterize14: Int,
	characterize15: Int,
	characterize16: Int,
	characterize17: Int,
	characterize18: Int
	)
case class ApplyQuestion (
	apply1: Int,
	apply2: Int,
	apply3: Int,
	apply4: Int,
	apply5: Int,
	apply6: Int,
	apply7: Int,
	apply8: Int,
	apply9: Int,
	apply10: Int,
	apply11: Int,
	apply12: Int,
	apply13: Int,
	apply14: Int,
	apply15: Int,
	apply16: Int,
	apply17: Int,
	apply18: Int,
	apply19: Int,
	apply20: Int,
	apply21: Int
	)

case class DescribeYouQuestion (
	describey1: Int,
	describey2: Int,
	describey3: Int,
	describey4: Int,
	describey5: Int,
	describey6: Int,
	describey7: Int,
	describey8: Int,
	describey9: Int,
	describey10: Int,
	describey11: Int,
	describey12: Int,
	describey13: Int,
	describey14: Int,
	describey15: Int,
	describey16: Int,
	describey17: Int,
	describey18: Int,
	describey19: Int,
	describey20: Int
	)

case class ExcessiveInternetSurvey (
	/** question 1 */
//	name : String,
	/** question 2 */
	gender: String,
	/** question3 */
	age : Int,
	/** question4 */
	internetAge: Int,
	/** question5 */
	averageHour: Int,
	/** question6 */
	locations :LocationUsage,
	homeHours: Option[Int],
	outsideHours: Option[Int],
	otherPlaces: Option[String],
	otherPlacesHours: Option[Int],
	/** question7 */
	doingOntNet: DoingOnNet,
	otherDoing: Option[String],
	/** question8 */
	onlineDevices: OnlineDevice,
	otherDevices: Option[String],
	/** question9 */
	characterizeQuestion: CharacterizeQuestion,
	/** question10 */
	applyQuestion: ApplyQuestion,
	/** question11 */
	describeYouQuestion: DescribeYouQuestion
	){
	def PIUQ: Double = {
		characterizeQuestion.getClass.getDeclaredFields.foldLeft(0.0) { (sum, f) => {
			f.setAccessible(true)
			sum + f.get(characterizeQuestion).asInstanceOf[Int]
			}
		}
	}
	def SIAS: Double = {
		//get question by name!!!!
		val q = for (question <- ExcessiveInternetSurvey.survey.questions if question.name == "describeYouQuestion";
			length = question.asInstanceOf[ScalingQuestion].scaling.length) yield length
		
		if (q.length > 0){
		//val lenght = ExcessiveInternetSurvey.survey.questions(9).asInstanceOf[ScalingQuestion].scaling.length
		describeYouQuestion.getClass.getDeclaredFields.zipWithIndex.foldLeft(0.0) { (sum, f) => {
			f._1.setAccessible(true)
			f._2 match {
				case x if Set(4, 8, 10) contains x => {
					sum + (q(0) - f._1.get(describeYouQuestion).asInstanceOf[Int])
				}
				case _ => sum + f._1.get(describeYouQuestion).asInstanceOf[Int]		
			}
		}
		}
		} else 0.0

	}
	def DASSDepresion: Double = {
		applyQuestion.getClass.getDeclaredFields.zipWithIndex.foldLeft(0.0) { (sum, f) => {
			f._1.setAccessible(true)
			f._2 match {
				case x if Set(0,5,7,10,11,13,17) contains x => {
					sum + f._1.get(applyQuestion).asInstanceOf[Int]
				}
				case _ => sum
			}
		}
		} * 2

	}
	def DASSAnxiety: Double = {
		applyQuestion.getClass.getDeclaredFields.zipWithIndex.foldLeft(0.0) { (sum, f) => {
			f._1.setAccessible(true)
			f._2 match {
				case x if Set(1,3,6,8,14,18,19) contains x => {
					sum + f._1.get(applyQuestion).asInstanceOf[Int]
				}
				case _ => sum
			}
		}
		} * 2
	}	
	def DASSStress: Double = {
		applyQuestion.getClass.getDeclaredFields.zipWithIndex.foldLeft(0.0) { (sum, f) => {
			f._1.setAccessible(true)
			f._2 match {
				case x if Set(2,4,9,12,15,16,20) contains x => {
					sum + f._1.get(applyQuestion).asInstanceOf[Int]
				}
				case _ => sum
			}
		}
		} * 2
	}
}

/** Required companion object for conversion of json to work
 *
 */
object AnswerJsonProtocol extends DefaultJsonProtocol {
  implicit val describeYouQuestionFormat = jsonFormat(
      DescribeYouQuestion,
      "describey1",
      "describey2",
      "describey3",
      "describey4",
      "describey5",
      "describey6",
      "describey7",
      "describey8",
      "describey9",
      "describey10",
      "describey11",
      "describey12",
      "describey13",
      "describey14",
      "describey15",
      "describey16",
      "describey17",
      "describey18",
      "describey19",
      "describey20"
  )
  implicit val applyQuestionFormat = jsonFormat(
      ApplyQuestion,
      "apply1",
      "apply2",
      "apply3",
      "apply4",
      "apply5",
      "apply6",
      "apply7",
      "apply8",
      "apply9",
      "apply10",
      "apply11",
      "apply12",
      "apply13",
      "apply14",
      "apply15",
      "apply16",
      "apply17",
      "apply18",
      "apply19",
      "apply20",
      "apply21"
  )
  implicit val characterizeQuestionFormat = jsonFormat(
      CharacterizeQuestion,
      "characterize1",
      "characterize2",
      "characterize3",
      "characterize4",
      "characterize5",
      "characterize6",
      "characterize7",
      "characterize8",
      "characterize9",
      "characterize10",
      "characterize11",
      "characterize12",
      "characterize13",
      "characterize14",
      "characterize15",
      "characterize16",
      "characterize17",
      "characterize18"
  )
  implicit val locationUsageFormat = jsonFormat(
      LocationUsage,
      "homeUsage",
      "hotspotUsage"
  )
  implicit val doingOnNetFormat = jsonFormat(
      DoingOnNet,
      "searchInfo",
      "readNews",
      "writeForum",
      "useSocial",
      "playGame"
  )
  implicit val onlineDeviceFormat = jsonFormat(
      OnlineDevice,
      "pc",
      "laptop",
      "tablet",
      "mobile"
  )
  implicit val excessiveInternetSurveyFormat = jsonFormat(
      ExcessiveInternetSurvey.apply,
      "gender",
      "age",
      "internetAge",
      "averageHour",
      "locations",
      "homeHours",
      "outsideHours",
      "otherPlaces",
      "otherPlacesHours",
      "doingOntNet",
      "otherDoing",
      "onlineDevices",
      "otherDevices",
      "characterizeQuestion",
      "applyQuestion",
      "describeYouQuestion"
  )
  implicit object SurveyAnswerFormat extends RootJsonFormat[SurveyAnswer[ExcessiveInternetSurvey]] {
    val dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    override def write(obj: SurveyAnswer[ExcessiveInternetSurvey]) = {
      JsObject(
          "surveyId" -> JsString(obj.surveyId),
          "email" -> JsString(obj.email),
          "time" -> JsString(obj.time.toString),
          "survey" -> obj.survey.toJson
      )
    }

    override def read(json: JsValue) : SurveyAnswer[ExcessiveInternetSurvey] = {
      json.asJsObject.getFields("surveyId", "email", "time", "survey") match {
        case Seq(JsString(surveyId), JsString(email), JsString(time), JsString(survey)) => {
          SurveyAnswer[ExcessiveInternetSurvey](surveyId, email, DateTime.parse(time, DateTimeFormat.forPattern(dateFormat)), survey.parseJson.convertTo[ExcessiveInternetSurvey])
        }
      }
    }
  }
}


object ExcessiveInternetSurvey {
	val survey = new Survey("ExcessiveInternetSurvey", Array(

	// new Question( index = 1, name = "name", text = "Name"){
	// 		questionStyles = Array(ShortTextQuestion(name = name, question = text))
	// 	},
	new Question( index = 1, name = "gender", text = "Gender"){
		questionStyles = Array(
			SingleMultipleChoice(name = name, question = text,
				choices = Array(
					SimpleChoice(name="Male", text="Male"), SimpleChoice(name="Female", text="Female"))
			)
		)},
	new Question( index = 2, name = "age", text = "Age"){
		questionStyles = Array(
			NumberQuestion(name = name, question = "Age"))
		},
	new Question( index = 3, name = "internetAge", 
		text ="How old were you when you started using the Internet?"){
			questionStyles = Array(NumberQuestion(name = name, question = text,  limit=Some((1, 150))))
		},
	new Question( index = 4, name = "averageHour", 
		text ="On average, how many hours do you spend on the Internet every week?"){
			questionStyles = Array(NumberQuestion(name = name, question = text))
		},
	new Question( index = 5, name = "locations", 
		text ="Where do you usually access the Internet? Please √ all that apply. For each location that you √, please indicate the average number of hours you spend on the Internet every week?"){
			questionStyles = Array(
				MultipleMultipleChoice(name = name, question = text,
					choices = Array(
						ComplexChoice(name="homeUsage",
							"Home/hostel/apartment", 
								Array(NumberQuestion(name="homeHours", question = "Hours"))),
						ComplexChoice(name="hotspotUsage",
							"Wi-Fi hotspots (e.g., café)", 
								Array(NumberQuestion(name="outsideHours", question = "Hours"))),
						ComplexChoice(name="otherPlaces",
							"Others", 
								Array(ShortTextQuestion(name="otherPlaces", question = "Others"),
										NumberQuestion(name="otherPlacesHours", question = "Hours")))
					)
				)
			)
		},
	new Question( index = 6, name = "doingOntNet", 
		text ="What do you usually do on the Internet? (√ all that apply)"){
			questionStyles = Array(
				MultipleMultipleChoice(name = name, question = text,
				choices = Array(
						SimpleChoice(name="searchInfo", "Search for information (e.g., for school work and other research)"),
						SimpleChoice(name="readNews", "Read news"),
						SimpleChoice(name="writeForum", "Read or write forums and blogs"),
						SimpleChoice(name="useSocial", "Use social networking sites (e.g., Facebook, MySpace)"),
						SimpleChoice(name="playgame", "Play online games"),
						ComplexChoice(name="otherWork", "Others (please specify)", 
							Array(ShortTextQuestion(name="otherDoing", question = "Others"))				
						)
					)
				)
			)
		},
	new Question( index = 7, name = "onlineDevices", 
		text ="Which of the following device(s) you usually use to access the Internet? (√ all that apply)"){
			questionStyles = Array(
				MultipleMultipleChoice(name = name, question = text,
					choices = Array(
						SimpleChoice(name="pc", "Desktop PCs"),
						SimpleChoice(name="laptop", "Laptops"),
						SimpleChoice(name="tablet", "Tablets (e.g., Ipad, Samsung GALAXY Note)"),
						SimpleChoice(name="mobile", "Mobile phones"),
						ComplexChoice(name="otherDevice", "Others (please specify)", 
							Array(ShortTextQuestion(name="otherDevices", question = "Others")))
					)
				)
			)
		},
	new ScalingQuestion( _index = 8, _name = "characterizeQuestion", 
		_text ="In the following you will read statements about your Internet use. Please indicate on a scale from 1 to 5 how much these statements characterize you.",
		_questionStyles = Array(
			ScalingStatement(name="characterize1", question = "How often do you fantasize about the Internet, or think about what it would be like to be online when you are not on the Internet?"),
			ScalingStatement(name="characterize2", question = "How often do you neglect household chores to spend more time online?"),
			ScalingStatement(name="characterize3", question = "How often do you feel that you should decrease the amount of time spent online?"),
			ScalingStatement(name="characterize4", question = "How often do you daydream about the Internet?"),
			ScalingStatement(name="characterize5", question = "How often do you spend time online when you’d rather sleep?"),
			ScalingStatement(name="characterize6", question = "How often does it happens to you that you wiash to decrease the amount of time spend online but you do not succeed?"),
			ScalingStatement(name="characterize7", question = "How often do you feel tense, irritated, or stressed if you cannot use the Internet for as long as you want to?"),
			ScalingStatement(name="characterize8", question = "How often do you choose the Internet rather than being with your partner?"),
			ScalingStatement(name="characterize9", question = "How often do you try to conceal the amount of time spent online?"),
			ScalingStatement(name="characterize10", question = "How often do you feel tense, irritated, or stressed if you cannot use the Internet for several days?"),
			ScalingStatement(name="characterize11", question = "How often does the use of Internet impair your work or your efficacy?"),
			ScalingStatement(name="characterize12", question = "How often do you feel that your Internet usage causes problems for you?"),
			ScalingStatement(name="characterize13", question = "How often does it happen to you that you feel depressed, moody, or nervous when you are not on the Internet and these feelings stop once you are back online?"),
			ScalingStatement(name="characterize14", question = "How often do people in your life complain about spending too much time online?"),
			ScalingStatement(name="characterize15", question = "How often do you realize saying when you are online, “just a couple of more minutes and I will stop”?"),
			ScalingStatement(name="characterize16", question = "How often do you dream about the Internet?"),
			ScalingStatement(name="characterize17", question = "How often do you choose the Internet rather than going out with somebody to have some fun?"),
			ScalingStatement(name="characterize18", question = "How often do you think that you should ask for help in relation to your Internet use?")																							
		),
		scaling = Array(
			ScalingValue("Never", 1),
		 	ScalingValue("Rarely", 2), 
		 	ScalingValue("Sometimes", 3), 
		 	ScalingValue("Often", 4), 
		 	ScalingValue("Always", 5))),
	new ScalingQuestion( _index = 9, _name = "applyQuestion", 
		_text ="Please read each statement and circle a number 0, 1, 2 or 3 which indicates how much the statement applied to you over the past week.  There are no right or wrong answers.  Do not spend too much time on any statement.",
		_questionStyles = Array(
			ScalingStatement(name = "apply1", question = "I found it hard to wind down"),
			ScalingStatement(name = "apply2", question = "I was aware of dryness of my mouth"),
			ScalingStatement(name = "apply3", question = "I couldn't seem to experience any positive feeling at all"),
			ScalingStatement(name = "apply4", question = "I experienced breathing difficulty (e.g., excessively rapid breathing, breathlessness in the absence of physical exertion)"),
			ScalingStatement(name = "apply5", question = "I found it difficult to work up the initiative to do things"),
			ScalingStatement(name = "apply6", question = "I tended to over-react to situations"),
			ScalingStatement(name = "apply7", question = "I experienced trembling (e.g., in the hands)"),
			ScalingStatement(name = "apply8", question = "I felt that I was using a lot of nervous energy"),
			ScalingStatement(name = "apply9", question = "I was worried about situations in which I might panic and make a fool of myself"),
			ScalingStatement(name = "apply10", question = "I felt that I had nothing to look forward to"),
			ScalingStatement(name = "apply11", question = "I found myself getting agitated"),
			ScalingStatement(name = "apply12", question = "I found it difficult to relax"),
			ScalingStatement(name = "apply13", question = "I felt down-hearted and blue"),
			ScalingStatement(name = "apply14", question = "I was intolerant of anything that kept me from getting on with what I was doing"),
			ScalingStatement(name = "apply15", question = "I felt I was close to panic"),
			ScalingStatement(name = "apply16", question = "I was unable to become enthusiastic about anything"),
			ScalingStatement(name = "apply17", question = "I felt I wasn't worth much as a person"),
			ScalingStatement(name = "apply18", question = "I felt that I was rather touchy"),
			ScalingStatement(name = "apply19", question = "I was aware of the action of my heart in the absence of physical exertion (eg, sense of heart rate increase, heart missing a beat)"),	
			ScalingStatement(name = "apply20", question = "I felt scared without any good reason"),
			ScalingStatement(name = "apply21", question = "I felt that life was meaningless")																	
		),
		scaling = Array(
			ScalingValue("Did not apply to me at all", 0),
		 	ScalingValue("Applied to me to some degree, or some of the time", 1), 
		 	ScalingValue("Applied to me to a considerable degree, or a good part of time", 2), 
		 	ScalingValue("Applied to me very much, or most of the time", 3)
		 )),
	new ScalingQuestion( _index = 10, _name = "describeYouQuestion", 
		_text ="THINK about the way you feel, think, and act most of the time and in most situations. For each question, please circle ONE number that best describes you.",
		_questionStyles = Array(
			ScalingStatement(name="describey1", question = "I get nervous if I have to speak with someone in authority (teacher, boss)"),
			ScalingStatement(name="describey2", question = "I have difficulty making eye contact with others"),
			ScalingStatement(name="describey3", question = "I become tense if I have to talk about my feelings or myself"),
			ScalingStatement(name="describey4", question = "I find it difficult to mix comfortably with the people I work with"),
			ScalingStatement(name="describey5", question = "I find it easy to make friends my own age"),
			ScalingStatement(name="describey6", question = "I tense up if I meet an acquaintance in the street"),
			ScalingStatement(name="describey7", question = "When mixing socially, I am uncomfortable"),
			ScalingStatement(name="describey8", question = "I feel tense when I am alone with just one person"),
			ScalingStatement(name="describey9", question = "I am at ease meeting people at parties, etc."),
			ScalingStatement(name="describey10", question = "I have difficulty talking with other people"),
			ScalingStatement(name="describey11", question = "I find it easy to think of things to talk about"),
			ScalingStatement(name="describey12", question = "I worry about expressing myself in case I appear awkward"),
			ScalingStatement(name="describey13", question = "I find it difficult to disagree with another’s point of view"),
			ScalingStatement(name="describey14", question = "I have difficulty talking to attractive persons of the opposite sex"),
			ScalingStatement(name="describey15", question = "I find myself worrying that I won’t know what to say in social situations"),
			ScalingStatement(name="describey16", question = "I am nervous mixing with people I don’t know well"),
			ScalingStatement(name="describey17", question = "I feel I’ll say something embarrassing when talking"),
			ScalingStatement(name="describey18", question = "When mixing in a group, I find myself worrying I will be ignored"),
			ScalingStatement(name="describey19", question = "I am tense mixing in a group"),	
			ScalingStatement(name="describey20", question = "I am unsure whether to greet someone I know only slightly")
		),
		scaling = Array(
			ScalingValue("Not at all", 0),
		 	ScalingValue("Slightly", 1), 
		 	ScalingValue("Moderately", 2), 
		 	ScalingValue("Very", 3),
		 	ScalingValue("Extremely", 4)
		 ))
	
	))
	
}