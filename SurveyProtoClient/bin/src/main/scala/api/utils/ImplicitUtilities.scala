package api.utils

import scala.concurrent.ExecutionContext
import com.typesafe.config.ConfigFactory

import java.io.File

import akka.actor.{ActorSystem}
import akka.stream.ActorMaterializer

/** Utility companion object with implicit values
 * 
 */
object ImplicitUtilities {
  implicit val system = ActorSystem("SurveyOfflineClient")
  implicit val materializer = ActorMaterializer()
  
  /** Method to close the Actor System and the Actor Materializer
   * @note call this before the application exit
   */
  def killAll{
    system.terminate
    materializer.shutdown
  }
}