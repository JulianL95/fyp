package api.utils

import java.util.UUID
import java.io.{File, FileReader, BufferedReader, PrintWriter, FileOutputStream, ObjectOutputStream, FileNotFoundException, ObjectInputStream, FileInputStream}

import scalafx.scene.control.{ TextInputDialog, DialogPane, ButtonType }
import scalafx.scene.input.MouseEvent
import scalafx.Includes._

import spray.json._

import api.models.questionaireanswermodel.ExcessiveInternetSurvey

object Utilities {
  
  /** Generate a UUID
   *
   */
  def generateUUID : String = {
    UUID.randomUUID.toString
  }
  
  /** A simpler version for textInputDialogs
   * 
   * @param titleDialog the text that is going to be appearing on the window top left corner
   * @param question the prompt you want to ask
   * @param header **OPTIONAL** the words on the top of the answer text field
   * @return The tupple returns are String for the answer the user input and Boolean for whether the user cancels it
   */
  def textInputDialog(titleDialog: String, question: String, header: String = ""): Option[String] = {
    val dialog = new TextInputDialog(new javafx.scene.control.TextInputDialog){
      title = titleDialog
      contentText = question
      headerText = header
    }
    val dialogRes = dialog.showAndWait
    dialogRes
  }
  
  /** A method to simplify the saving of jsonString data
   * @note If path is null it saves at the path of the executable which calls this method
   * @note If Filename is null it generates a random id and attach the filetype to the end of the name for this method the filetype will be .json
   * @return a boolean to state whether or not the saving was sucessfull
   */
  def saveLocally(path: String, nameOfFile: String, jsonString: String): Boolean = {
    var fileName = nameOfFile
    if (nameOfFile.equals("") || nameOfFile == null) fileName = generateUUID
    try {
      val file = new File(path +  File.separator  + fileName + ".json")
      val writer = new PrintWriter(file)
      writer.print(jsonString)
      writer.close
      true
    } catch {
      case e: NullPointerException => {
        val file = new File(fileName + ".json")
        val writer = new PrintWriter(file)
        writer.print(jsonString)
        writer.close
        true
      }
      case e: Exception => e.printStackTrace; false; 
    }
  }
  
  /** A method to simplify the saving of object data
   * @note If path is null it saves at the path of the executable which calls this method
   * @note If Filename is null it generates a random id and attach the filetype to the end of the name for this method the filetype will be .json
   * @return a boolean to state whether or not the saving was sucessfull
   */
  def saveLocally[T<:Any](path: String, nameOfFile: String, answerObject: T): Boolean = {
    var fileName = nameOfFile
    if (nameOfFile.equals("") || nameOfFile == null) fileName = generateUUID
    var fout: FileOutputStream = null;
		var oos: ObjectOutputStream = null;

		try {
			fout = new FileOutputStream(path + File.separator  + fileName + ".srl");
			oos = new ObjectOutputStream(fout);
			oos.writeObject(answerObject)
			true
		} catch {
		  case e: FileNotFoundException => {
		    fout = new FileOutputStream(new File(fileName + ".srl"));
  			oos = new ObjectOutputStream(fout);
  			oos.writeObject(answerObject)
  			true
		  }
		  case e: Exception => false
		} finally {
			if (fout != null) {
				try {
					fout.close();
				} catch {
				  case e:Exception => 
				}
			}
			if (oos != null) {
				try {
					oos.close();
				} catch {
				  case e:Exception => 
				}
			}
		}
  }
  
  /** A method to load .srl files into object specify by the generic [T]
   * @param path the path to the file
   * @param nameOfFile the name of the file
   * @return the object or None if there is an exception
   */
  def loadSerializedObject[T<:Any](path: String, nameOfFile: String): Option[T] = {
    var returnVal: Option[T]= None
    try{
      val inputFileStream = new FileInputStream(path + File.separator  + nameOfFile + ".srl");
      val objectInputStream = new ObjectInputStream(inputFileStream);
      val deserializedObject = objectInputStream.readObject.asInstanceOf[T]
      objectInputStream.close
      inputFileStream.close
      returnVal = Some(deserializedObject)
      returnVal
    } catch {
      case e: Exception =>
        e.printStackTrace
        returnVal
    }
  }
}