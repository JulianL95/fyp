name := "SurveyProtoClient"

version := "1.0"

scalaVersion := "2.12.2"

offline := true
resolvers += "Local Maven Repository" at "file:///"+Path.userHome+ "/.ivy2/cache"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

unmanagedJars in Compile += Attributed.blank(file(System.getenv("JAVA_HOME") + "/jre/lib/ext/jfxrt.jar"))

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

libraryDependencies ++= Seq(
  "org.scalafx" %% "scalafx" % "8.0.102-R11",
  "org.scalafx" %% "scalafxml-core-sfx8" % "0.3",
  "com.github.nscala-time" %% "nscala-time" % "2.16.0",
  "com.typesafe.akka" %% "akka-http" % "10.0.0" ,
  "com.typesafe.akka" %% "akka-actor" % "2.4.17",
  "com.typesafe.akka" %% "akka-remote" % "2.4.17",
  "com.typesafe.akka" %% "akka-typed-experimental" % "2.4.17",
  "io.spray" %%  "spray-json" % "1.3.3"
)

mainClass in assembly := Some("prototype.Prototype")

EclipseKeys.executionEnvironment := Some(EclipseExecutionEnvironment.JavaSE18)

fork := true

connectInput in run := true
